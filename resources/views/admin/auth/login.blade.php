<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-login.css') }}">
</head>

<body>

<div class="container">
    <div class="sign_in_view">
        <img id="profile-img" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" class="profile-img-card">
        <p id="profile-name" class="profile-name-card"></p>
        <form class="login-box" method="post" action="{{ route('login_admin_post') }}">
            @csrf

            <div class="has-danger">
                <div class="form-control-feedback">{{ session('message') }}</div>
            </div>

            <div class="form-group">
                <input type="email" name="email" id="inputEmail" placeholder="Email address" value="{{ old('email') }}"
                       required="required" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" name="password" id="inputPassword" placeholder="Password" required="required"
                       class="form-control">
            </div>
            <div class="form-group">
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin">Sign In</button>
        </form>
    </div>
</div>


</body>
</html>