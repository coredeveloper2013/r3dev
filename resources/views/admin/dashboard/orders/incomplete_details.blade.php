@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .table .table {
            background-color: white;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" href="{{ url()->previous() }}">Back to List</a>
        </div>
    </div>

    <br>

    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="product-thumbnail"><b>Image</b></th>
                <th><b>Style No.</b></th>
                {{-- <th class="text-center"><b>Color</b></th>
                <th class="text-center"><b>Size</b></th>
                <th class="text-center"><b>Pack</b></th> --}}
                <th class="text-center"><b>Total Qty</b></th>
                <th class="text-center"><b>Unit Price</b></th>
                <th class="text-center"><b>Amount</b></th>
            </tr>

            <tbody>
            <?php
            $totalItem = 0;
            $total = 0;
            ?>
            @foreach ( $allItems as $item )
                <tr>
                    <td>
                        @if ( isset($item['image']) )
                            <img src="{{ asset($item['image']) }}" alt="Product" height="100px">
                        @else
                            <img src="{{ asset('images/no-image.png') }}" alt="Product" height="100px">
                        @endif
                    </td>

                    <td>
                        {{ $item['style_no'] }}
                    </td>
                    <td>
                        {{ $item['total_qty'] }}
                    </td>
                    <td>
                        {{ $item['price'] }}
                    </td>
                    <td>
                        {{ $item['amount'] }}
                        <?php $total += $item['amount']; ?>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row" style="margin-top: 20px">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <div class="table-responsive">
                <table class="table table-bordered">
                    {{-- <tr>
                        <th>Total Item</th>
                        <td>{{ 0 }}</td>
                    </tr> --}}

                    <tr>
                        <th>Total</th>
                        <td>${{ number_format($total, 2, '.', '') }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@stop