<?php use App\Enumeration\VendorImageType; ?>
@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-4">
            <div class="col-12 text-right" style="margin-bottom: 20px;padding-right: 15px">
                <img src="{{ asset($black->value) }}" alt="" width="150px" style="border: 1px solid #c8c8c8;padding: 15px">
                <br>
            </div>
            <div class="col-12">
                <form action="{{ route('admin_logo_add_post') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-2">
                            <label class="col-form-label">Logo:</label>
                        </div>
                        <div class="col-10">
                            <input type="file" class="form-control{{ $errors->has('logo2') ? ' is-invalid' : '' }}" name="logo2" accept="image/*">

                            @if ($errors->has('logo2'))
                                <div class="form-control-feedback">{{ $errors->first('logo2') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 text-right">
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
