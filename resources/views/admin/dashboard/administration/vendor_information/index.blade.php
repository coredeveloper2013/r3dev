@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .checkbox-inline {
            display: inline-flex;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="global_tab vendor_info_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="Company-tab" data-toggle="tab" href="#Company" role="tab" aria-controls="Company" aria-selected="true">Company Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Size-tab" data-toggle="tab" href="#Size" role="tab" aria-controls="Size" aria-selected="false">Size Chart</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Order-tab" data-toggle="tab" href="#Order" role="tab" aria-controls="Order" aria-selected="false">Order Notice</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Setting-tab" data-toggle="tab" href="#Setting" role="tab" aria-controls="Setting" aria-selected="false">Setting</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Company" role="tabpanel" aria-labelledby="Company-tab">
                        <div class="tab_content">
                            @include('admin.dashboard.administration.vendor_information.includes.company_info')
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Size" role="tabpanel" aria-labelledby="Size-tab">
                        <div class="tab_content">
                            @include('admin.dashboard.administration.vendor_information.includes.size_chart')
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Order" role="tabpanel" aria-labelledby="Order-tab">
                        <div class="tab_content">
                            @include('admin.dashboard.administration.vendor_information.includes.order_notice')
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Setting" role="tabpanel" aria-labelledby="Setting-tab">
                        <div class="tab_content">
                            @include('admin.dashboard.administration.vendor_information.includes.settings')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;
            var showroomStateId = '{{ $user->vendor->billing_state_id }}';
            var warehouseStateId = '{{ $user->vendor->factory_state_id }}';

            $('#showroom_country').change(function () {
                var countryId = $(this).val();
                $('#showroom_state').html('<option value="">Select State</option>');

                if (countryId == 1) {
                    $.each(usStates, function (index, value) {
                        if (value.id == showroomStateId)
                            $('#showroom_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#showroom_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                } else if (countryId == 2) {
                    $.each(caStates, function (index, value) {
                        if (value.id == showroomStateId)
                            $('#showroom_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#showroom_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                }
            });

            $('#warehouse_country').change(function () {
                var countryId = $(this).val();
                $('#warehouse_state').html('<option value="">Select State</option>');

                if (countryId == 1) {
                    $.each(usStates, function (index, value) {
                        if (value.id == warehouseStateId)
                            $('#warehouse_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#warehouse_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                } else if (countryId == 2) {
                    $.each(caStates, function (index, value) {
                        if (value.id == warehouseStateId)
                            $('#warehouse_state').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                        else
                            $('#warehouse_state').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                }
            });

            $('#showroom_country').trigger('change');
            $('#warehouse_country').trigger('change');

//            var sizeEditor = CKEDITOR.replace( 'size_chart_editor' );
//            var orderNotice = CKEDITOR.replace( 'order_notice_editor' );
            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ route('unisharp.lfm.upload') }}?type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };
            CKEDITOR.replace('size_chart_editor', options);
            CKEDITOR.replace('order_notice_editor', options);

            $('#btnSizeChartSubmit').click(function (e) {
                e.preventDefault();
                var description = sizeEditor.getData();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_size_chart_post') }}",
                    data: { description: description }
                }).done(function( msg ) {
                    toastr.success("Size Chart Updated!");
                });
            });

            $('#btnOrderNoticeSubmit').click(function (e) {
                e.preventDefault();
                var description = orderNotice.getData();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_order_notice_post') }}",
                    data: { description: description }
                }).done(function( msg ) {
                    toastr.success("Order Notice Updated!");
                });
            });


            $('#btnSaveSettings').click(function (e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_save_setting_post') }}",
                    data: $('#form-settings').serialize(),
                }).done(function( data ) {
                    if (data.success) {
                        toastr.success("Settings Saved!");
                    } else {
                        alert(data.message);
                    }
                });
            });
        })
    </script>
@stop