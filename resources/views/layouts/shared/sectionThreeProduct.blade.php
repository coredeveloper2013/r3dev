@if(count($sectionThree) > 0)
    <section class="home_product_area">
        <div class="main_title">
            <div class="container custom_container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>{{ $sectionThreeTitle->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container custom_container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="product3_slider" class="owl-carousel owl-theme">
                        @foreach($sectionThree as $item)
                            <div class="product_inner text-center">
                                @if(count($item['images']) > 0)
                                    <a href="{{ route('item_details_page', $item['slug']) }}"><img src="{{asset('/'.$item['images'][0]['image_path'])}}" alt=""
                                                     class="img-fluid"></a>
                                @else
                                    <a href="{{ route('item_details_page', $item['slug']) }}"><img src="{{asset('/images/no-image.png')}}" alt=""
                                                     class="img-fluid"></a>
                                @endif
                                <div class="product_description">
                                    <h2 class="name">{{$item['name']}}</h2>
                                    <p class="description">{{$item['description']}}</p>
                                    <p class="price">
                                        @if($item['orig_price'] != null && $item['orig_price'] != '')
                                            <span>${{$item['orig_price']}}</span>
                                        @endif
                                        ${{$item['price']}}</p>
                                        <button class="btn" onclick="linkToDetailPage('{{ route('item_details_page',  $item['slug']) }}')">Shop Now</button>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </section>
@endif