<header class="header_area fixed-top">
    <div class="header_top">
        <div class="container custom_container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header_top_info">{!! $top_notification !!}</div>

                    {{--<div class="header_top_info"><p>FREE U.S. SHIPPING ON ORDERS OVER $25 <span>See details*</span></p></div>--}}
                </div>
                <div class="col-lg-6">
                    <div class="header_top_menu">
                        <ul>
                            @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role == 3)
                                <li><a href="{{route('logout_buyer')}}">Sign Out</a></li>
                                <li><a href="{{route('buyer_show_overview')}}">My Account</a></li>
                            @else
                                <li><a href="{{route('buyer_login')}}">Sign In </a></li>
                                <li><a href="{{route('buyer_login')}}">Register</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container menu_container custom_container">
        <div id="menuzord" class="menuzord">
            <a href="{{route('home')}}" class="menuzord-brand"><img src="{{$black_logo_path}}" alt=""></a>
            <ul class="menuzord-menu for_desktop menuzord-menu-bg">
                <li><a href="#"><span>Shop</span></a>
                    <div class="megamenu megamenu-full-width megamenu-bg">
                        <div class="megamenu-row">
                            @foreach($default_categories as $cat)
                                <div class="mega-item col3">
                                    <h2><a href="{{ route('category_page', $cat['slug']) }}">{{ $cat['name'] }}</a></h2>
                                    <ul>
                                        @foreach($cat['subCategories'] as $subCat)
                                            <li>
                                                <a href="{{ route('second_category', ['parent'=>$cat['slug'], 'category'=>$subCat['slug']]) }}">{{ $subCat['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </li>
                <li><a href="{{ route('our_story') }}"><span>Our Story</span></a>
                    {{--<div class="megamenu megamenu-full-width megamenu-bg">
                        <div class="megamenu-row">
                            <div class="mega-item col3">
                                <ul>
                                    <li><a href="{{ route('our_story') }}">Our Story</a></li>
                                </ul>
                            </div>
                            <div class="mega-item col3">
                                <ul>
                                    <li><a href="{{ route('contact_us') }}">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>--}}
                </li>
            </ul>
            <div class="header_cart">
                <ul>
                    <li id="header_search_ic" onclick="openSearch()"><img src="{{asset('/images/search_icon.svg')}}"
                                                                          alt=""></li>
                    <li><a href="{{route('show_cart')}}"><img src="{{asset('/images/shopping_bag.svg')}}" alt=""> <span
                                    id="cartCountNow">{{$cart_items['total']['total_qty']}}</span></a></li>
                </ul>
            </div>
            <ul class="menuzord-menu for_desktop menuzord-right">
                <li><a href="{{ route('product_page') }}"><span>Product</span></a></li>
                <li><a href="{{ route('contact_us') }}"><span>Contact</span></a></li>
            </ul>
        </div>
    </div>

    <div class="for_mobile">
        <div class="mobile_overlay"></div>
        <span class="ic_c_mb">
                <img src="{{asset('/images/cross_ic.png')}}" alt=""></span>
        <div class="mobile_menu">
            <div class="top_menu_list clearfix">
                <ul>
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <li><a href="{{route('logout_buyer')}}" class="mobile-list-item-link">Sign Out</a></li>
                        <li><a href="{{route('buyer_show_overview')}}" class="mobile-list-item-link">My Account</a></li>
                    @else
                        <li><a href="{{route('buyer_login')}}" class="mobile-list-item-link">Sign In </a></li>
                        <li><a href="{{route('buyer_login')}}" class="mobile-list-item-link">Register</a></li>
                    @endif
                </ul>
            </div>
            <div class="menu-list clearfix">
                <ul id="menu-content" class="menu-content">
                    {{--<div>
                        <li class="no-before"><a href="{{route('home')}}">Home</a></li>
                    </div>--}}
                    <div>
                        <li class="no-before"><a href="{{route('product_page')}}">All Products</a></li>
                    </div>

                    @foreach($default_categories as $cat)
                        <div>
                            @if(count($cat['subCategories']) > 0)
                                <li data-toggle="collapse" data-target="#cat{{$cat['id']}}"
                                    class="collapsed">{{ $cat['name'] }}</li>
                                <ul id="cat{{$cat['id']}}" class="sub-menu collapse clearfix">
                                    {{--<li data-toggle="collapse" data-target="#expandUl_{{$cat['id']}}"--}}
                                        {{--class="sub_collapse collapsed">--}}
                                        {{--<div class="display-in-bl">--}}
                                            {{--<a href="{{ route('category_page', $cat['slug']) }}"--}}
                                               {{--class="mobile-list-item-link">{{ $cat['name'] }}</a>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    @foreach($cat['subCategories'] as $subCat)
                                        <li class="mobile-list-item">
                                            <a href="{{ route('second_category', ['parent'=>$cat['slug'], 'category'=>$subCat['slug']]) }}"
                                               class="mobile-list-item-link"> {{$subCat['name']}} </a>
                                        </li>
                                    @endforeach
                                    {{--<ul id="expandUl_{{$cat['id']}}" class="collapse clearfix">--}}
                                        {{--@foreach($cat['subCategories'] as $subCat)--}}
                                            {{--<li class="mobile-list-item">--}}
                                                {{--<a href="{{ route('second_category', ['parent'=>$cat['slug'], 'category'=>$subCat['slug']]) }}"--}}
                                                   {{--class="mobile-list-item-link"> {{$subCat['name']}} </a>--}}
                                            {{--</li>--}}
                                        {{--@endforeach--}}
                                    {{--</ul>--}}
                                </ul>
                            @else
                                <li class="no-before"><a
                                            href="{{ route('category_page', $cat['slug']) }}">{{ $cat['name'] }}</a>
                                </li>
                            @endif
                        </div>
                    @endforeach
                    <div>
                        <li data-toggle="collapse" data-target="#Other" class="collapsed">Other Pages</li>
                        <ul id="Other" class="sub-menu collapse clearfix">
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('our_story')}}" class="mobile-list-item-link">Our Story</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('contact_us')}}" class="mobile-list-item-link">Contact Us</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('billing_shipping')}}" class="mobile-list-item-link">Shipping &
                                        Delivery</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('return_info')}}" class="mobile-list-item-link">Return Policy</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('privacy_policy')}}" class="mobile-list-item-link">Privacy
                                        Policy</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('terms_conditions')}}" class="mobile-list-item-link">Terms &
                                        Conditions</a>
                                </div>
                            </li>
                            <li class="no-before">
                                <div class="display-in-bl">
                                    <a href="{{route('faq')}}" class="mobile-list-item-link">Faqs</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</header>
{{--
<div class="special_offer">
    <div class="container custom_container">
        <div class="row">
            <div class="col">
                <div class="special_offer_left">
                    <p>Celebrate 12 Days of Magic!</p>
                </div>
            </div>
            <div class="col text-right">
                <div class="special_offer_right">
                    <a href="#">DISCOVER TODAY'S SPECIAL</a>
                </div>
            </div>
        </div>
    </div>
</div>--}}
<div class="header_search" style="display: none;">
    <div class="header_search_inner">
        <form action="{{route('search')}}" method="get">
            <input type="text" placeholder="What can we help you find?" name="s" class="form-control"
                   value="@if(isset($_GET['s'])){{$_GET['s']}}@endif">
            <button type="submit"><img src="{{asset('/images/search_icon.svg')}}" alt=""></button>
            <span><img src="{{asset('/images/x_icon.svg')}}" alt=""></span>
        </form>
    </div>
</div>
<div class="cartGlobal">
    @if($cart_items['total']['total_qty'] > 0)
        <div class="cart_fixed_bottom">
            <ul>
                <li><a href="{{route('show_cart')}}"><img src="{{asset('/images/shopping_bag.svg')}}" alt="">
                        <span>{{$cart_items['total']['total_qty']}}</span></a></li>
                <li><a href="{{route('show_cart')}}">see my item</a></li>
            </ul>
            <span onclick="closeCart(this)">Close</span>
        </div>
    @endif
</div>


<script type="application/javascript">
    function addToCart(id) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '{{route('add_to_cart')}}',
            type: 'post',
            data: {itemId: id, qty: 1},
            success: function (res) {
                if (res.status === 2000) {
                    var th = `<div class="cart_fixed_bottom">
                                <ul>
                                    <li><a href="{{route('show_cart')}}"><img src="{{asset('/images/shopping_bag.svg')}}" alt=""> <span>` + res.count + `</span></a></li>
                                    <li><a href="{{route('show_cart')}}">see my item</a></li>
                                </ul>
                                <span onclick="closeCart(this)">Close</span>
                            </div>`;
                    $('.cartGlobal').html(th);
                    $('#cartCountNow').html(res.count);
                }
            }
        });

    }

    function closeCart(trigger) {
        $(trigger).closest('.cartGlobal').html('');
    }

    function openSearch() {
        $('.header_search').fadeIn();
    }

    function linkToDetailPage(detailPageLink) {
        window.location.href = detailPageLink;
    }

</script>
