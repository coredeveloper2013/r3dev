<!-- =========================
    START FOOTER SECTION
============================== -->
<footer class="footer_area">
    <div class="container custom_container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_menu">
                    <ul>
                        <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                        <li><a href="{{ route('faq') }}">FAQs</a></li>
                        <li><a href="{{ route('billing_shipping') }}">Shipping & Delivery</a></li>
                        <li><a href="{{ route('return_info') }}">Return Policy</a></li>
                        <!-- <li><a href="{{ route('return_info') }}">Affiliates</a></li> -->
                        <!-- <li><a href="{{ route('return_info') }}">Nail Parlors</a></li> -->
                        <li>
                            <span style="margin: 2px auto;display: table;" id="google_translate_element"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="sign_up_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sign_up_inner">
                        <input type="text" placeholder="Enter your email for R3 All news & offers" class="form-control">
                        <button class="button"><img src="{{asset('/images/bannerarrowright.png')}}" alt=""></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container custom_container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_social">
                    <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer_copyright">
                    <ul>
                        <li><span>© 2018 R3All</span></li>
                        <li><a href="{{ route('terms_conditions') }}">Terms & Conditions</a></li>
                        <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="scrollToTop" onclick="goTop()"><i class="fa fa-chevron-circle-up"></i></div>
<script>
    function goTop(){
        $('html,body').animate({ scrollTop: 0 }, 400);
    }
    setTimeout(function(){
        $(function(){
            $(window).scroll(function(e){
                if(window.scrollY > 100){
                    $('#scrollToTop').fadeIn();
                } else {
                    $('#scrollToTop').fadeOut();
                }
            });
        })
    }, 1000);
</script>

<script>
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'auto',
            includedLanguages: 'en,ja,ko,zh-CN,zh-TW',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
            autoDisplay: false
        }, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>