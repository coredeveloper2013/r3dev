
<section class="banner_area">
    <div id="banner_slider" class="owl-carousel owl-theme home-slider-top-margin">
        @foreach($mainSliderImages as $slide)
        <div class="banner_item">
            <a href="{{$slide['url']}}"><img src="{{asset($slide['image_path'])}}" alt="" class="img-fluid"></a>
        </div>
        @endforeach
    </div>            
</section>