<?php

use App\Enumeration\Role;
use App\Enumeration\Availability;

?>

@extends('layouts.home_layout')

@section('additionalCSS')

@stop

@section('content')
    <!-- =========================
        START CART SECTION
    ============================== -->
    <section class="shopping_cart_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product_title cart_main_title">
                        <p>Add your special touch with free artist-inspired printable gift tags. Download them <a
                                    href="#"><b>here.</b></a></p>
                        <h2>Shopping Bag</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart_table show_desktop">
                        <div class="table-responsive ">
                            @if($errors->has('error')) <p
                                    class="alert alert-danger">{{$errors->first('error')}}</p> @endif
                            @if($errors->has('message')) <p
                                    class="alert alert-success">{{$errors->first('message')}}</p> @endif
                            <form action="{{route('update_cart')}}" method="post" class="cartListForm">
                                @csrf
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit Price</th>
                                        <th>Qty</th>
                                        <th>Subtotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $ci = 0; @endphp
                                    @foreach($cartItems as $cart)
                                        <tr>
                                            <td>
                                                @if (isset($cart['item']['images']) && count($cart['item']['images']) > 0 )
                                                    <a class="cartListItems"
                                                       href="{{ route('item_details_page', $cart['item']['slug']) }}"><img
                                                                src="{{asset('/' . $cart['item']['images'][0]['image_path'])}}"
                                                                alt="" class="img-fluid"></a>
                                                @else
                                                    <a class="cartListItems"
                                                       href="{{ route('item_details_page', $cart['item']['slug']) }}"><img
                                                                src="{{asset('/images/no-image.png')}}" alt=""
                                                                class="img-fluid"></a>
                                                @endif
                                                <a href="{{ route('item_details_page', $cart['item']['slug']) }}">
                                                    <h2>{{$cart['item']['name']}}</h2>
                                                    <p>{{$cart['item']['style_no']}}</p>
                                                </a>
                                                <a href="javascript:void(0)" onclick="removeThisCart(this)"
                                                   class="cart_small">Remove</a>
                                            </td>
                                            <td>${{$cart['item']['price']}}</td>
                                            <td><input type="number" name="cart[{{$ci}}][qty]" class="form-control"
                                                       onchange="changeCartQty(this)"
                                                       placeholder="1" value="{{$cart['quantity']}}"><input
                                                        type="hidden" name="cart[{{$ci}}][itemId]"
                                                        value="{{$cart['item']['id']}}"></td>
                                            <td>${{$cart['item']['price'] * $cart['quantity']}}</td>
                                        </tr>
                                        @php $ci++; @endphp
                                    @endforeach
                                    <tr>
                                        <td colspan="2"><a href="{{route('product_page')}}" class="continue_shopping">Continue
                                                Shopping</a></td>
                                        <td colspan="2">
                                            <button type="submit" class="continue_shopping"> Update Shopping Bag
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <form action="{{route('add_to_cart_promo')}}" method="post">
                                    @csrf
                                    <div class="cart_bottom_inner">
                                        <label>Enter your promo code</label>
                                        <input type="text" class="form-control" name="code">
                                        <button type="submit">Apply</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <div class="cart_bottom_inner"> &nbsp;
                                    {{--<label>Enter your gift card code</label>
                                    <input type="text" class="form-control">
                                    <button>Apply</button>
                                    <a href="#">Check Your Gift Card Balance</a>--}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="cart_total_area">
                                    <div class="cart_total">
                                        <ul>
                                            <li><span>Subtotal</span> <span>${{$subTotal}}</span></li>
                                            @if(isset($promo) && $promo > 0)
                                                <li><span>Promo Discount</span> <span>-${{$promo}}</span></li>
                                            @endif
                                            @if(isset($giftCard) && $giftCard > 0)
                                                <li><span>Gift Card Discount</span> <span>-${{$giftCard}}</span></li>
                                            @endif
                                            <li><span>Total</span> <span>${{$total}}</span></li>
                                        </ul>
                                    </div>
                                    <a href="{{route('show_checkout')}}" class="btn_common_type2">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cart_table show_mobile">
                        @if($errors->has('error')) <p
                                class="alert alert-danger">{{$errors->first('error')}}</p> @endif
                        @if($errors->has('message')) <p
                                class="alert alert-success">{{$errors->first('message')}}</p> @endif
                        <form action="{{route('update_cart')}}" method="post" class="cartListForm">
                            @csrf
                            @php $ci = 0; @endphp
                            @foreach($cartItems as $cart)
                                <div class="cart_table cart_table_mobile clearfix">
                                    <div class="cart_product">
                                        @if (isset($cart['item']['images']) && count($cart['item']['images']) > 0 )
                                            <a class="cartListItems"
                                               href="{{ route('item_details_page', $cart['item']['slug']) }}"><img
                                                        src="{{asset('/' . $cart['item']['images'][0]['image_path'])}}"
                                                        alt="" class="img-fluid"></a>
                                        @else
                                            <a class="cartListItems"
                                               href="{{ route('item_details_page', $cart['item']['slug']) }}"><img
                                                        src="{{asset('/images/no-image.png')}}" alt=""
                                                        class="img-fluid"></a>
                                        @endif
                                    </div>
                                    <div class="cart_product_desc">
                                        <a href="{{ route('item_details_page', $cart['item']['slug']) }}"
                                           class="cart_product_title">{{$cart['item']['name']}}</a>
                                        <a href="{{ route('item_details_page', $cart['item']['slug']) }}"
                                           class="cart_product_subtitle">Mystery Box</a>
                                        <p>Unit price: <span> ${{$cart['item']['price']}}</span></p>
                                        <p>Subtotal: <span> ${{$cart['item']['price'] * $cart['quantity']}}</span></p>

                                        <div class="input-group cart_input_number">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn-number" onclick="changeCartQtyAdd(this,'minus')">
                                            -
                                        </button>
                                    </span>
                                            <input type="number" name="cart[{{$ci}}][qty]" class="input-number"
                                                   onchange="changeCartQty(this)"
                                                   data-min="1" data-max="1000"
                                                   placeholder="1" value="{{$cart['quantity']}}"><input
                                                    type="hidden" name="cart[{{$ci}}][itemId]"
                                                    value="{{$cart['item']['id']}}">
                                            <span class="input-group-btn">
                                        <button type="button" class="btn-number minus_btn"
                                                onclick="changeCartQtyAdd(this,'add')">
                                            +
                                        </button>
                                    </span>
                                        </div>
                                        <a href="javascript:void(0)" onclick="removeThisCart(this)" class="cart_small">Remove</a>
                                    </div>
                                </div>
                                @php $ci++; @endphp
                            @endforeach
                            <div class="clearfix"></div>
                            <div class="continue_shipping_mobile clearfix">
                                <a href="{{route('product_page')}}" class="continue_shopping">Continue Shopping</a>
                                <button type="submit" class="continue_shopping"> Update Shopping Bag</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-4">
                                <form action="{{route('add_to_cart_promo')}}" method="post">
                                    @csrf
                                    <div class="cart_bottom_inner">
                                        <label>Enter your promo code</label>
                                        <input type="text" class="form-control" name="code">
                                        <button type="submit">Apply</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <div class="cart_bottom_inner"> &nbsp;
                                    {{--<label>Enter your gift card code</label>
                                    <input type="text" class="form-control">
                                    <button>Apply</button>
                                    <a href="#">Check Your Gift Card Balance</a>--}}
                                </div>
                                <div class="cart_total_mobile">
                                    <ul>
                                        <li>
                                            <span>Subtotal</span>
                                            <span style="float: right;">${{$subTotal}}</span>
                                        </li>
                                        @if(isset($promo) && $promo > 0)
                                            <li>
                                                <span>Promo Discount</span>
                                                <span style="float: right;">-$ <span id="__promo">{{ $promo }}</span>
                                            </li>
                                        @endif
                                        @if(isset($giftCard) && $giftCard > 0)
                                            <li>
                                                <span>Gift Card Discount</span>
                                                <span style="float: right;">-${{$giftCard}}</span>
                                            </li>
                                        @endif
                                        <li>
                                            <span>Total</span>
                                            <span style="float: right;">${{$total}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="cart_total_area checkout_btn_mobile">
                                <a href="{{route('show_checkout')}}" class="btn_common_type2">Checkout</a>
                                {{--<a href="#" class="show_mobile">Enter Promo or Gift Card Code</a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END CART SECTION
    ============================== -->
    <script type="application/javascript">
        function removeThisCart(trigger) {
            if (confirm('are you really want to remove this item from cart?')) {
                var tr = $(trigger).closest('tr');
                var form = $(trigger).closest('.cartListForm');
                tr.remove();
                setTimeout(function(){
                    form.submit();
                },500);
            }
        }

        function changeCartQtyAdd(trigger, type) {
            var form = $(trigger).closest('.cartListForm');
            if (type === 'add') {
                var target = $(trigger).closest('.cart_product_desc').find('.input-number');
                var max = target.attr('data-max');
                var min = target.attr('data-min');
                var v = parseInt(target.val()) + 1;
                if (v > max) {
                    v = max
                }
                if (v < min) {
                    v = min
                }
                target.val(v);
            }
            if (type === 'minus') {
                var target = $(trigger).closest('.cart_product_desc').find('.input-number');
                var max = target.attr('data-max');
                var min = target.attr('data-min');
                var v = parseInt(target.val()) - 1;
                if (v > max) {
                    v = max
                }
                if (v < min) {
                    v = min
                }
                target.val(v);
            }
            setTimeout(function(){
                form.submit();
            },500);
        }
        function changeCartQty(trigger){
            var form = $(trigger).closest('.cartListForm');
            setTimeout(function(){
                form.submit();
            },500);
        }
    </script>
@stop

@section('additionalJS')

@stop