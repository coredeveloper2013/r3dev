@extends('layouts.home_layout')

@section('content')


    <!-- =========================
            START  SIGN IN AREA SECTION
        ============================== -->
    <section class="sign_in_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-6 sign_up_right_padding">
                    <h2>Password Recovery</h2>
                    <div class="sign_in_inner">
                        <h3>Please enter your new password</h3>

                        @if (session('message'))
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                        @endif

                        <form method="post" action="{{route('reset_password_buyer_now' , ['token' => $token])}}">
                            @csrf
                            <div class="form-group">
                                {{--<label>Email *</label>--}}
                                <span class="text-danger">{{$errors->has('password') ? $errors->first('password') : ''}}</span>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                {{--<label>Email *</label>--}}
                                <span class="text-danger">{{$errors->has('confirm_password') ? $errors->first('confirm_password') : ''}}</span>
                                <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                            </div>
                            <button type="submit" class="btn_common">Reset</button>
                        </form>
                        {{--<p class="p_required">* Required</p>--}}
                        <h1>Need Help?</h1>
                        <p>We're here Monday-Friday, 10 am-5 pm ET</p>
                        <br>
                        <p><b>877-651-3292</b></p>
                        <a href="mailto:pjglobalr3@gmail.com" class="sign_email">pjglobalr3@gmail.com</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END SIGN IN AREA SECTION
    ============================== -->



@endsection