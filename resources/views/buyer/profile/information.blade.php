@extends('layouts.home_layout')

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        @include('buyer.profile.menu')

                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>My Information</h2>
                        </div>
                        <div class="my_info_area">
                            <h2>Contact Information <a href="{{route('buyer_edit_shipping_info')}}">Edit</a></h2>
                            @if(auth()->user())
                            <p>{{auth()->user()->first_name}} &nbsp; {{auth()->user()->last_name}}</p>
                            <p>{{auth()->user()->email}}</p>
                            @endif
                            <p><a href="{{route('new_password_buyer')}}">Change Password</a></p>
                            <p><a href="{{route('buyer_get_change_avatar')}}">Upload Avatar</a></p>

                            <h2>Shipping Information <a href="{{route('buyer_edit_shipping_info')}}">Edit</a></h2>
                            <p>{{$buyerInfo->company_name}}</p>
                            <p>{{$buyerInfo->billing_location}}</p>
                            <address>
                                <p>{{(isset($buyerInfo->address1) ? $buyerInfo->address1 : '')}}</p>
                                <p>{{(isset($buyerInfo->address2) ? $buyerInfo->address2 : '')}}</p>
                                <p>{{($buyerInfo->city) ? $buyerInfo->city : ''}}  {{(isset($buyerInfo->state)) ? " , ".$buyerInfo->state : ''}} {{(isset($buyerInfo->zip))? " , ".$buyerInfo->zip : '' }}</p>
                            </address>
                            <p>{{(isset($buyerInfo->country)) ? "Country: ".$buyerInfo->country : ''}}</p>
                            <p>{{ (isset($buyerInfo->mobile)) ? "Telephone: ".$buyerInfo->mobile : '' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
