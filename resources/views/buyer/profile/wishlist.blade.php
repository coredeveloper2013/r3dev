@extends('layouts.home_layout')

@section('additionalCSS')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
@stop

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                     @include('buyer.profile.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content my_wishlist_area">
                        <div class="myaccount_title">
                            <h2>My Wishlist</h2>
                        </div>
                        <div class="my_info_area my_wishlist_area">
                            {{-- <button class="btn_common">add all to bag</button>
                            <button class="btn_common">share wishlist</button>
                            <p>Confetti Ready has been added to your wishlist. Click <a href="{{ route('product_page') }}">here</a> to continue shopping.</p> --}}
                            
                            <div class="row">
                                @if ( ! $items->isEmpty() )
                                    @foreach ( $items as $product )
                                    <div class="col-lg-4">
                                        <div class="product_wishlist text-center">
                                            <form action="{{ route('remove_from_wishlist') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="item_id" value="{{ $product->id }}">
                                                <input type="submit" class="remove-wishlist-list" value="X">
                                            </form>
                                            {{-- <button onclick="myFunction();"><img src="{{ asset('images/cross_ic.png') }}" alt="No image"></button> --}}
                                            @if ( isset($product->images) && count($product->images) > 0 )
                                                <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{ asset('/' . $product->images[0]['image_path']) }}" alt="" class="img-fluid"></a>
                                            @else
                                                <a href="{{ route('item_details_page', $product->slug) }}"><img src="{{ asset('/images/no-image.png') }}" alt="" class="img-fluid"></a>
                                            @endif
                                            <div>
                                                <h2>{{ $product->name }}</h2>
                                                <p class="price">
                                                    @if($product->orig_price != null && $product->orig_price != '')
                                                    <span>${{$product->orig_price}}</span>
                                                    @endif
                                                    ${{ $product->price }}
                                                </p>
                                                <a class="btns" href="javascript:void(0)" onclick="addToCart('{{$product->id}}')"> add to bag</a>
                                            </div>
                                            @if ( $product->rating['count'] > 0 )
                                                <div class="product_star_rating">
                                                    <span class="customer_rateYo" data-type="{{ $product->rating['rate'] }}"></span>
                                                    <span class="average_rating">({{ $product->rating['count'] }})</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>    
                                    @endforeach
                                @else 
                                    <p class="no_wishlist">You have no items in your wishlist.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
@section('additionalJS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script>
    //Make sure that the dom is ready
    $(function () {
        
        $(".customer_rateYo").each(function(){
            var rating = $(this).attr('data-type');
            $(this).rateYo({
                starWidth: "17px",
                rating: rating,
                readOnly: true,
                normalFill: "#cfcfcf",
                ratedFill: "#000"
            });
        });

    });
</script>
@stop