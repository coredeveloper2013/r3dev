@extends('layouts.home_layout')

@section('content')

    <section class="my_account_area common_top_margin">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-3">
                    <div class="my_accout_menu">
                        <h2>MY ACCOUNT</h2>
                        <ul>
                            <li><a href="{{ route('buyer_show_overview') }}">My Dashboard</a></li>
                            <li><a href="{{ route('buyer_my_information') }}">My Information</a></li>
                            <li><a href="{{ route('buyer_billing') }}">Billing Information</a></li>
                            <li><a href="{{ route('buyer_show_orders') }}">Order History</a></li>
                            {{-- <li><a href="#">My Auto Replenishments</a></li> --}}
                            <li><a href="{{ route('view_wishlist') }}">My Wishlist</a></li>
                            <li class="active"><a href="{{ route('buyer_beauty_bio') }}">My Beauty Bio</a></li>
                            <li><a href="{{ route('buyer_gift_card') }}">Check Gift Card Balance</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="my_account_content">
                        <div class="myaccount_title">
                            <h2>My Beauty Bio</h2>
                        </div>
                        <div class="my_info_area">
                            <br>
                            <p>Take a few minutes to tell us a little about yourself—your answers will help us create even more products you'll love.</p> <br>
                        </div>
                        <div class="my_beauty_bio_area">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-About-tab" data-toggle="tab" href="#nav-About" role="tab" aria-controls="nav-About" aria-selected="true">About Me</a>
                                    <a class="nav-item nav-link" id="nav-Polish-tab" data-toggle="tab" href="#nav-Polish" role="tab" aria-controls="nav-Polish" aria-selected="false">Polish</a>
                                    <a class="nav-item nav-link" id="nav-Beauty-tab" data-toggle="tab" href="#nav-Beauty" role="tab" aria-controls="nav-Beauty" aria-selected="false">Beauty Products</a>
                                    <a class="nav-item nav-link" id="nav-Summary-tab" data-toggle="tab" href="#nav-Summary" role="tab" aria-controls="nav-Summary" aria-selected="false">Summary</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-About" role="tabpanel" aria-labelledby="nav-About-tab">
                                    <div class="my_beauty_inner">
                                        <div class="my_beauty_radio_wrapper">
                                            <h2>What color are your cs?</h2>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye1" name="eye" checked>
                                                <label for="eye1">
                                                    <img src="images/my-account/beauty-bio1.jpg" class="img-fluid" alt="">
                                                    <span>Black</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye2" name="eye">
                                                <label for="eye2">
                                                    <img src="images/my-account/beauty-bio2.jpg" class="img-fluid" alt="">
                                                    <span>Dark brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye3" name="eye">
                                                <label for="eye3">
                                                    <img src="images/my-account/beauty-bio3.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye4" name="eye">
                                                <label for="eye4">
                                                    <img src="images/my-account/beauty-bio4.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye5" name="eye">
                                                <label for="eye5">
                                                    <img src="images/my-account/beauty-bio5.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="eye6" name="eye">
                                                <label for="eye6">
                                                    <img src="images/my-account/beauty-bio6.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                            <h2>What color is your hair?</h2>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair1" name="hair" checked>
                                                <label for="hair1">
                                                    <img src="images/my-account/beauty-bio7.jpg" class="img-fluid" alt="">
                                                    <span>Black</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair2" name="hair">
                                                <label for="hair2">
                                                    <img src="images/my-account/beauty-bio8.jpg" class="img-fluid" alt="">
                                                    <span>Dark brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair3" name="hair">
                                                <label for="hair3">
                                                    <img src="images/my-account/beauty-bio9.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair4" name="hair">
                                                <label for="hair4">
                                                    <img src="images/my-account/beauty-bio10.jpg" class="img-fluid" alt="">
                                                    <span>Blonde</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair5" name="hair">
                                                <label for="hair5">
                                                    <img src="images/my-account/beauty-bio11.jpg" class="img-fluid" alt="">
                                                    <span>Red</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair6" name="hair">
                                                <label for="hair6">
                                                    <img src="images/my-account/beauty-bio12.jpg" class="img-fluid" alt="">
                                                    <span>Gray</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="hair6" name="hair">
                                                <label for="hair6">
                                                    <img src="images/my-account/beauty-bio13.jpg" class="img-fluid" alt="">
                                                    <span>Other</span>
                                                </label>
                                            </div>
                                            <h2>What color is your skin tone?</h2>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin1" name="skin" checked>
                                                <label for="skin1">
                                                    <img src="images/my-account/beauty-bio14.jpg" class="img-fluid" alt="">
                                                    <span>Fair</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin2" name="skin">
                                                <label for="skin2">
                                                    <img src="images/my-account/beauty-bio15.jpg" class="img-fluid" alt="">
                                                    <span>Light</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin3" name="skin">
                                                <label for="skin3">
                                                    <img src="images/my-account/beauty-bio16.jpg" class="img-fluid" alt="">
                                                    <span>Medium</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin4" name="skin">
                                                <label for="skin4">
                                                    <img src="images/my-account/beauty-bio17.jpg" class="img-fluid" alt="">
                                                    <span>Tan/Olive</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin5" name="skin">
                                                <label for="skin5">
                                                    <img src="images/my-account/beauty-bio18.jpg" class="img-fluid" alt="">
                                                    <span>Medium Dark</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="skin6" name="skin">
                                                <label for="hair6">
                                                    <img src="images/my-account/beauty-bio19.jpg" class="img-fluid" alt="">
                                                    <span>Dark</span>
                                                </label>
                                            </div>

                                            <h2>Describe your skin type and main concerns.</h2>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="undertone1" name="undertone" checked>
                                                <label for="undertone1">
                                                    <span>Golden (warm): undertone tans easily with a hint of gold and brown</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="undertone2" name="undertone">
                                                <label for="undertone2">
                                                    <span>Pink (cool): Burns or flushes easily with a hint of pink</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="undertone3" name="undertone">
                                                <label for="undertone3">
                                                    <span>Hints of both golden and pink</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="undertone4" name="undertone">
                                                <label for="undertone4">
                                                    <span>Can't tell/not sure</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="my_beauty_checkbox_wrapper">
                                            <h2>Describe your skin type and main concerns.</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns1">
                                                <label for="concerns1">Normal: no major concerns</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns2">
                                                <label for="concerns2">Dry</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns3">
                                                <label for="concerns3">Oily</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns4">
                                                <label for="concerns4">Combination – dry</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns5">
                                                <label for="concerns5">Combination – oily</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns6">
                                                <label for="concerns6">Sensitive</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="concerns7">
                                                <label for="concerns7">Acne-prone</label>
                                            </div>
                                        </div>
                                        <div class="my_beauty_radio_wrapper">
                                            <h2>What is your age?</h2>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="age1" name="age" checked>
                                                <label for="age1">
                                                    <span>19 or younger</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="age2" name="age">
                                                <label for="age2">
                                                    <span>20-29</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="age3" name="age">
                                                <label for="age3">
                                                    <span>30-39</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="age4" name="age">
                                                <label for="age4">
                                                    <span>40-49</span>
                                                </label>
                                            </div>
                                            <h2>What would you like in your monthly box?</h2>
                                            <p>Not a subscriber? Get started by taking the <a href="#">beauty quiz</a> </p>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="box1" name="box" checked>
                                                <label for="box1">
                                                    <img src="images/my-account/beauty-bio20.jpg" class="img-fluid" alt="">
                                                    <span>Black</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="box2" name="box">
                                                <label for="box2">
                                                    <img src="images/my-account/beauty-bio21.jpg" class="img-fluid" alt="">
                                                    <span>Dark brunette</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button">
                                                <input type="radio" id="box3" name="box">
                                                <label for="box3">
                                                    <img src="images/my-account/beauty-bio22.jpg" class="img-fluid" alt="">
                                                    <span>Light brunette</span>
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn_common_type2">next</button>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-Polish" role="tabpanel" aria-labelledby="nav-Polish-tab">
                                    <div class="my_beauty_inner">
                                        <div class="my_beauty_checkbox_wrapper">
                                            <h2>Which polish colors do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish1">
                                                <label for="polish1">
                                                    <img src="images/my-account/beauty-bio23.jpg" class="img-fluid" alt="">
                                                    <span>Nudes</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish2">
                                                <label for="polish2">
                                                    <img src="images/my-account/beauty-bio24.jpg" class="img-fluid" alt="">
                                                    <span>Classics</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish3">
                                                <label for="polish3">
                                                    <img src="images/my-account/beauty-bio25.jpg" class="img-fluid" alt="">
                                                    <span>Brights</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish4">
                                                <label for="polish4">
                                                    <img src="images/my-account/beauty-bio26.jpg" class="img-fluid" alt="">
                                                    <span>Darks</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish5">
                                                <label for="polish5">
                                                    <img src="images/my-account/beauty-bio27.jpg" class="img-fluid" alt="">
                                                    <span>Metallics</span>
                                                </label>
                                            </div>
                                            <h2>Which polish finishes do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try1">
                                                <label for="try1">
                                                    <img src="images/my-account/beauty-bio28.jpg" class="img-fluid" alt="">
                                                    <span>Matte</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try2">
                                                <label for="try2">
                                                    <img src="images/my-account/beauty-bio29.jpg" class="img-fluid" alt="">
                                                    <span>Crème</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try3">
                                                <label for="try3">
                                                    <img src="images/my-account/beauty-bio30.jpg" class="img-fluid" alt="">
                                                    <span>Shimmer</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try4">
                                                <label for="try4">
                                                    <img src="images/my-account/beauty-bio31.jpg" class="img-fluid" alt="">
                                                    <span>Glitter</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try5">
                                                <label for="try5">
                                                    <img src="images/my-account/beauty-bio32.jpg" class="img-fluid" alt="">
                                                    <span>Special Effect</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try6">
                                                <label for="try6">
                                                    <img src="images/my-account/beauty-bio33.jpg" class="img-fluid" alt="">
                                                    <span>Metallicl Effect</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="my_beauty_radio_wrapper">
                                            <h2>Do you experiment with new polish colors & finishes?</h2>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="experiment1" name="experiment" checked>
                                                <label for="experiment1">
                                                    <span>Rarely – I like to keep it consistent</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="experiment2" name="experiment">
                                                <label for="experiment2">
                                                    <span>Somewhat Adventurous – I like to experiment once in a while</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="experiment3" name="experiment">
                                                <label for="experiment3">
                                                    <span>No Limits – I want to try them all!</span>
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn_common_type2">next</button>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-Beauty" role="tabpanel" aria-labelledby="nav-Beauty-tab">
                                    <div class="my_beauty_inner">
                                        <div class="my_beauty_checkbox_wrapper">
                                            <h2>Which eyeshadow colors do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish1">
                                                <label for="polish1">
                                                    <img src="images/my-account/beauty-bio34.jpg" class="img-fluid" alt="">
                                                    <span>Classic/Neutral</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish2">
                                                <label for="polish2">
                                                    <img src="images/my-account/beauty-bio35.jpg" class="img-fluid" alt="">
                                                    <span>Dark/Smoky</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="polish3">
                                                <label for="polish3">
                                                    <img src="images/my-account/beauty-bio36.jpg" class="img-fluid" alt="">
                                                    <span>Bright/Bold</span>
                                                </label>
                                            </div>
                                            <h2>Which polish finishes do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try1">
                                                <label for="try1">
                                                    <img src="images/my-account/beauty-bio37.jpg" class="img-fluid" alt="">
                                                    <span>Matte</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try2">
                                                <label for="try2">
                                                    <img src="images/my-account/beauty-bio38.jpg" class="img-fluid" alt="">
                                                    <span>Crème</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try3">
                                                <label for="try3">
                                                    <img src="images/my-account/beauty-bio39.jpg" class="img-fluid" alt="">
                                                    <span>Shimmer</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try4">
                                                <label for="try4">
                                                    <img src="images/my-account/beauty-bio40.jpg" class="img-fluid" alt="">
                                                    <span>Glitter</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try5">
                                                <label for="try5">
                                                    <img src="images/my-account/beauty-bio41.jpg" class="img-fluid" alt="">
                                                    <span>Special Effect</span>
                                                </label>
                                            </div>
                                            <h2>Which polish finishes do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try1">
                                                <label for="try1">
                                                    <img src="images/my-account/beauty-bio42.jpg" class="img-fluid" alt="">
                                                    <span>Matte</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try2">
                                                <label for="try2">
                                                    <img src="images/my-account/beauty-bio43.jpg" class="img-fluid" alt="">
                                                    <span>Crème</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try3">
                                                <label for="try3">
                                                    <img src="images/my-account/beauty-bio44.jpg" class="img-fluid" alt="">
                                                    <span>Shimmer</span>
                                                </label>
                                            </div>
                                            <div class="form-group my_beauty_checkbox">
                                                <input type="checkbox" id="try4">
                                                <label for="try4">
                                                    <img src="images/my-account/beauty-bio45.jpg" class="img-fluid" alt="">
                                                    <span>Glitter</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="my_beauty_checkbox_wrapper">
                                            <h2>Which face products do you love or want to try?</h2>
                                            <p>(Select all that apply)</p>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products1">
                                                <label for="products1">Concealer</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products2">
                                                <label for="products2">Foundation</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products3">
                                                <label for="products3">Highlighter</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products4">
                                                <label for="products4">Bronzer</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products5">
                                                <label for="products5">Contour</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products6">
                                                <label for="products6">Blush</label>
                                            </div>
                                            <div class="form-group form_checkbox">
                                                <input type="checkbox" id="products7">
                                                <label for="products7">Primer</label>
                                            </div>
                                        </div>
                                        <div class="my_beauty_radio_wrapper">
                                            <h2>Do you experiment with your makeup look?</h2>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="emakeup1" name="emakeup" checked>
                                                <label for="emakeup1">
                                                    <span>Rarely – I like to keep it consistent</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="emakeup2" name="emakeup">
                                                <label for="emakeup2">
                                                    <span>Somewhat Adventurous – I like to emakeup once in a while</span>
                                                </label>
                                            </div>
                                            <div class="my_beauty_radio_button my_beauty_radio_button_full_width">
                                                <input type="radio" id="emakeup3" name="emakeup">
                                                <label for="emakeup3">
                                                    <span>No Limits – I want to try them all!</span>
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn_common_type2">next</button>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-Summary" role="tabpanel" aria-labelledby="nav-Summary-tab">
                                    <div class="my_beauty_inner">
                                        <h2>About Me</h2>
                                        <ul>
                                            <li>My eye color is: <span> ______</span></li>
                                            <li>My hair color is: <span> ______</span></li>
                                            <li>My skin tone is: <span> ______</span></li>
                                            <li>My undertone is: <span> ______</span></li>
                                            <li>My skin type is: <span> ______</span></li>
                                            <li>My age is:  <span>19 or younger</span></li>
                                            <li>I would like my monthly box to have: <span>Mix of both</span></li>
                                        </ul>
                                        <h2>Polish</h2>
                                        <ul>
                                            <li>I love these polish colors: <span> ______</span></li>
                                            <li>I love these polish finishes: <span> ______</span></li>
                                            <li>I experiment with new polish colors and finishes:<span>No Limits – I want to try them all!</span></li>
                                        </ul>
                                        <h2>Beauty Products</h2>
                                        <ul>
                                            <li>I love these eyeshadow colors: <span> ______</span></li>
                                            <li>I love these lip colors:<span> ______</span></li>
                                            <li>I love these cheek colors:<span>______</span></li>
                                            <li>I love these face products:<span>______</span></li>
                                            <li>I experiement with my makeup look:<span>______</span></li>
                                        </ul> <br>
                                        
                                        <button class="btn_common_type2">save</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
