<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'order_id', 'item_id', 'color', 'size', 'item_per_pack', 'pack', 'qty', 'total_qty', 'per_unit_price', 'amount',
        'style_no', 'dispatch'
    ];

    public function item() {
        return $this->belongsTo('App\Model\Item')->withTrashed();
    }

    public function itemImages() {
        return $this->belongsTo('App\Model\ItemImages','item_id','item_id')->orderBy('id','desc');
    }

    public function order() {
        return $this->belongsTo('App\Model\Order');
    }
}
