<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'color_id', 'quantity'
    ];

    public function item() {
        return $this->belongsTo('App\Model\Item')->with('images', 'vendor');
    }

    public function color() {
        return $this->belongsTo('App\Model\Color');
    }
}
