<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/5/2019
 * Time: 1:21 AM
 */

namespace App\Enumeration;


class Featured
{
    public static $BANNER_ONE = 1;
    public static $BANNER_TWO = 2;
    public static $BANNER_THREE = 3;
}