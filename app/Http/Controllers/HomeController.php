<?php

namespace App\Http\Controllers;

use App\Enumeration\Featured;
use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\Banners;
use App\Model\BodySize;
use App\Model\Category;
use App\Model\Item;
use App\Model\ItemCategory;
use App\Model\ItemImages;
use App\Model\ItemView;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\MetaVendor;
use App\Model\Pattern;
use App\Model\Setting;
use App\Model\Style;
use App\Model\VendorImage;
use App\Model\Visitor;
use App\Model\WishListItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index() {
        // Main Slider
        $mainSliderImages = VendorImage::where('type', VendorImageType::$MAIN_SLIDER)
            ->orderBy('sort')
            ->get()->toArray();

        // New Arrivals
        $sectionOneTitle = DB::table('featured')->where('id', 1)->get()->first();
        $sectionOne = Item::where('status', 1)
            ->where('featured', 1)
            ->where('min_qty', '>', 0)
            ->orderBy('sorting')
            ->with('images')
            ->limit(12)
            ->get()->toArray();
        $sectionTwoTitle = DB::table('featured')->where('id', 2)->get()->first();
        $sectionTwo = Item::where('status', 1)
            ->where('featured', 2)
            ->where('min_qty', '>', 0)
            ->orderBy('sorting')
            ->with('images')
            ->limit(12)
            ->get()->toArray();
        $sectionThreeTitle = DB::table('featured')->where('id', 3)->get()->first();
        $sectionThree = Item::where('status', 1)
            ->where('featured', 3)
            ->where('min_qty', '>', 0)
            ->orderBy('sorting')
            ->with('images')
            ->limit(12)
            ->get()->toArray();
//        dd($sectionOne,$sectionTwo, $sectionThree);

        // Promotion Items
        $promotionItems = Item::where('status', 1)
            ->whereNotNull('orig_price')
            ->orderBy('activated_at', 'desc')
            ->with('images')
            ->limit(12)
            ->get();

        // Best Selling Items
        $sql = "SELECT items.id, items.name, items.style_no, items.description, items.price, items.orig_price, t.count
                FROM items
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items GROUP BY item_id) t ON items.id = t.item_id
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 6";
        $bestItems = DB::select($sql);
        foreach ($bestItems as &$item) {
            $image = ItemImages::where('item_id', $item->id)
                ->orderBy('sort')
                ->first();
            $imagePath = asset('images/no-image.png');
            if ($image)
                $imagePath = asset($image->image_path);

            $item->image_path = $imagePath;
        }

        // Banners
        $bannerOne = Banners::where('name', 1)->orderBy('id','asc')->get()->toArray();
        $bannerOneDes = Banners::where('name', 111)->get()->first();
        $bannerTwo = Banners::where('name', 2)->orderBy('id','asc')->get()->toArray();
        $bannerTwoDes = Banners::where('name', 222)->get()->first();

        // Visitor
        Visitor::create([
            'user_id' => (Auth::check() && Auth::user()->role == Role::$BUYER) ? Auth::user()->id : null,
            'url' => url()->current(),
            'ip' => '3'
        ]);

        return view('pages.home', compact('mainSliderImages', 'promotionItems',
            'sectionOne',
            'sectionTwo',
            'sectionThree',
            'bannerOne',
            'bannerTwo',
            'bestItems',
            'bannerOneDes',
            'sectionOneTitle',
            'sectionTwoTitle',
            'sectionThreeTitle',
            'bannerTwoDes'));
    }

    public function newArrivalHome( Request $request) {

    }

    // public function itemDetails(Item $item) 
    public function itemDetails($itemSlug) 
    {
        // Get category slug by category id
        $itemId = 0;
        $slugCheck = Item::where('slug', $itemSlug)->first();
        if ( $slugCheck != null ) {
            $itemId = $slugCheck->id;
        }


        $item = Item::with('images')->where('id', $itemId)->first();



        $review = DB::table('product_reviews')
                        ->leftJoin('users', 'users.id', '=', 'product_reviews.customer_id')
                        ->leftJoin('meta_buyers', 'meta_buyers.user_id', '=', 'product_reviews.customer_id')
                        ->where('product_reviews.product_id', $itemId)
                        ->get();

        $wishlistFlag = 0;
        if ( isset(Auth::user()->id) ) {
            // Check this customer wishlist this product or not
            $get_customer_info = DB::table('wish_list_items')
                                    ->where('user_id', Auth::user()->id)
                                    ->where('item_id', $itemId)
                                    ->get()
                                    ->toArray();

            if ( count($get_customer_info) > 0 ) {
                $wishlistFlag = 1;
            }
        }

        
        // Best Selling Items
        $sql = "SELECT items.id, items.slug, item_images.image_path, items.name, items.style_no, items.description, items.price, items.orig_price, t.count
                FROM items
                LEFT JOIN item_images ON item_images.item_id = items.id 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items GROUP BY item_id) t ON items.id = t.item_id
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 6";
        $productByCatId = DB::select($sql);
        
        $i = 0;
        foreach ( $productByCatId as $items ) {
            // Get rating count
            $getRatingCount = DB::table('product_reviews')->where('product_id', $items->id)->get()->toArray();

            // Get rating summation
            $getRatingSum = DB::table('product_reviews')->where('product_id', $items->id)->sum('rating');
            
            if ( count($getRatingCount) > 0 ) {
                $getRatingSum = ( $getRatingSum / count($getRatingCount) );   
            }
            else {
                $getRatingSum = 0;
            }

            $productByCatId[$i]->rating = array(
                'count' => count($getRatingCount),
                'rate' => $getRatingSum
            );
            $i++;
        }

        // $productByCatId = DB::table('items')
        //                 ->select('*')
        //                 ->leftJoin('item_images', 'items.id', '=', 'item_images.item_id')
        //                 ->leftJoin('order_items', 'items.id', '=', 'order_items.item_id')
        //                 ->where('items.default_parent_category', $model->default_parent_category)
        //                 ->where('min_qty', '>', 0)
        //                 ->where('status', 1)
        //                 ->take(6)
        //                 ->get();
        //                 dd($productByCatId);


        //return $item;

        if(count($review) > 0)
            $average_rating =  collect($review)->avg('rating');
        else
            $average_rating = 0;

        
        return view('pages.item_details', compact('item', 'review', 'wishlistFlag', 'productByCatId' , 'average_rating'));
    }

    public function storeRating(Request $request)
    {
        $title = $request->title;
        $comment = $request->comment;
        $rate = is_null($request->rate_value)? 0 : $request->rate_value;
        $product_id = $request->product_id;

        // Check this customer rate it before it or not
        $get_customer_info = DB::table('product_reviews')
                                ->where('customer_id', Auth::user()->id)
                                ->where('product_id', $product_id)
                                ->get()
                                ->toArray();
        
        $data = [
            'title' => $title,
            'comment' => $comment,
            'rating' => $rate,
            'product_id' => $product_id,
            'customer_id' => Auth::user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        if ( count($get_customer_info) > 0 ) {
            // Update rating
            DB::table('product_reviews')
                ->where('product_id', $product_id)
                ->update($data);
        }
        else {
            // Insert new rating    
            DB::table('product_reviews')->insert($data);    
        }        

        return redirect()->back();
    }

	public function quickViewItemDetails(Request $request) {

    	$item = Item::where('id', $request->item)->first();

		$item->load('images', 'colors');

		foreach ($item->images as $image) {
		    $image->image_path = asset($image->image_path);
		    $image->list_image_path = asset($image->list_image_path);
		    $image->thumbs_image_path = asset($image->thumbs_image_path);
        }

        foreach ($item->colors as &$color) {
            $thumb = null;
            $index = 0;

            for($i=0; $i < sizeof($item->images); $i++) {
                if ($item->images[$i]->color_id == $color->id) {
                    $thumb = $item->images[$i];
                    $index = $i;
                    break;
                }
            }

            if ($thumb) {
                $color->image = asset($thumb->list_image_path);
                $color->image_index = $index;
            } else {
                $color->image = '';
            }
        }

		if (!Auth::check() || Auth::user()->role != Role::$BUYER) {
		    $item->price = '';
		    $item->style_no = '';
        }

		// Suggensted items
		/*$suggestItemsQuery = Item::query();

		if ($item->default_third_category != null || $item->default_third_category != '')
			$suggestItemsQuery->where('default_third_category', $item->default_third_category);
		else if ($item->default_second_category != null || $item->default_second_category != '')
			$suggestItemsQuery->where('default_second_category', $item->default_second_category);
		else
			$suggestItemsQuery->where('default_parent_category', $item->default_parent_category);

		$suggestItems = $suggestItemsQuery->where('status', 1)->limit(10)->inRandomOrder()->get();*/


		// Vendor
		$vendor = MetaVendor::where('id', 1)->first();

		return ['item' => $item, 'vendor' => $vendor];

        // return view('pages.item_details', compact('item', 'sizes', 'itemInPack', 'vendor'));
    }
    
    public function ProductPage(Request $request)
    {
        $input = $request->input();
        $limit = 12;
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : 'sorting';
        $order_by = isset($input['order_by']) ? $input['order_by'] : 'asc';

        $products = Item::with('images')->where('status', 1)->where('min_qty', '>', 0)->orderBy($sort_by, $order_by)->paginate($limit);
        $i = 0;
        foreach ( $products as $item ) {
            // Get rating count
            $getRatingCount = DB::table('product_reviews')->where('product_id', $item->id)->get()->toArray();

            // Get rating summation
            $getRatingSum = DB::table('product_reviews')->where('product_id', $item->id)->sum('rating');
            
            if ( count($getRatingCount) > 0 ) {
                $getRatingSum = ( $getRatingSum / count($getRatingCount) );   
            }
            else {
                $getRatingSum = 0;
            }

            $products[$i]['rating'] = array(
                'count' => count($getRatingCount),
                'rate' => $getRatingSum
            );
            $i++;
        }

        return view('pages.product_page', compact('products'));
    }

    public function CategoryPage(Request $request, $category)
    {
        $input = $request->input();
        $limit = 12;
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : 'sorting';
        $order_by = isset($input['order_by']) ? $input['order_by'] : 'asc';

        // Get category slug by category id
        $categoryId = 0;
        $slugCheck = Category::where('slug', $category)->first();
        if ( $slugCheck != null ) {
            $categoryId = $slugCheck->id;
        }

        $products = Item::with('images')
                        ->where('status', 1)
                        ->where('default_parent_category', $categoryId)
                        ->where('min_qty', '>', 0)
                        ->orderBy($sort_by, $order_by)
                        ->paginate($limit);
        $i = 0;
        foreach ( $products as $item ) {
            // Get rating count
            $getRatingCount = DB::table('product_reviews')->where('product_id', $item->id)->get()->toArray();

            // Get rating summation
            $getRatingSum = DB::table('product_reviews')->where('product_id', $item->id)->sum('rating');
            
            if ( count($getRatingCount) > 0 ) {
                $getRatingSum = ( $getRatingSum / count($getRatingCount) );   
            }
            else {
                $getRatingSum = 0;
            }

            $products[$i]['rating'] = array(
                'count' => count($getRatingCount),
                'rate' => $getRatingSum
            );
            $i++;
        }

        // Fetch category name by category id frmo url segment
        $categoryName = Category::where('id', $categoryId)->get()->toArray();

	    /*$categories = Category::where('parent', '=', 0)->get();

        $c = null;

	    foreach ($categories as $cat) {
		    if (changeSpecialChar($cat->name) == $category) {
                $c = $cat;
			    break;
		    }
	    }

	    if ($c) {

	    	// New Modify
            $category = $c;
//		    $category->subCategories = Category::where('parent', $c->id)->get();
//		    $newArrivals = Item::where([['default_parent_category', '=', $c->id], ['status', '=', 1]])
//		                       ->orderBy('activated_at', 'desc')
//		                       ->limit(14)
//		                       ->with('images', 'vendor', 'colors')
//		                       ->get();
//		    $masterColors = MasterColor::orderBy('name')->get();
//
//		    return view('pages.category_page', compact('newArrivals', 'category', 'masterColors'));
		    // New Modify

		    // ****************************************** //

		    $category->load('subCategories', 'lengths', 'parentCategory');

		    $patterns = Pattern::where('parent_category_id', $category->id)
		                       ->orderBy('name')
		                       ->get();

		    $styles = Style::where('parent_category_id', $category->id)
		                   ->orderBy('name')
		                   ->get();

		    $masterColors = MasterColor::orderBy('name')->get();
		    $masterFabrics = MasterFabric::orderBy('name')->get();

		    $items = [];
		    /*$items = Item::where('status', 1)
				->where('default_third_category', $category->id)
				->orderBy('activated_at', 'desc')
				->with('vendor', 'images', 'colors')
				->paginate(30);*/

		    // Wishlist
            /*$obj = new WishListItem();
            $wishListItems = $obj->getItemIds();*/

		    //return view('pages.catalog_category', compact('category', 'masterColors','masterFabrics', 'items', 'wishListItems', 'patterns', 'styles'));
		    // ****************************************** //
	    /*} else {
	    	abort(404);
        }*/

        return view('pages.catalog_category', compact('products', 'categoryName'));
    }

    public function secondCategory(Request $request, $parent, $category)
    {
        $input = $request->input();
        $limit = 12;
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : 'sorting';
        $order_by = isset($input['order_by']) ? $input['order_by'] : 'asc';

        // Get parent category slug by category id
        $parentCategoryId = 0;
        $slugCheck = Category::where('slug', $parent)->first();
        if ( $slugCheck != null ) {
            $parentCategoryId = $slugCheck->id;
        }
        
        // Get category slug by category id
        $categoryId = 0;
        $slugCheck = Category::where('slug', $category)->first();
        if ( $slugCheck != null ) {
            $categoryId = $slugCheck->id;
        }

        $products = Item::with('images')
                        ->where('status', 1)
                        ->where('default_second_category', $categoryId)
                        ->where('min_qty', '>', 0)
                        ->orderBy($sort_by, $order_by)
                        ->paginate($limit);
        $i = 0;
        foreach ( $products as $item ) {
            // Get rating count
            $getRatingCount = DB::table('product_reviews')->where('product_id', $item->id)->get()->toArray();

            // Get rating summation
            $getRatingSum = DB::table('product_reviews')->where('product_id', $item->id)->sum('rating');
            
            if ( count($getRatingCount) > 0 ) {
                $getRatingSum = ( $getRatingSum / count($getRatingCount) );   
            }
            else {
                $getRatingSum = 0;
            }

            $products[$i]['rating'] = array(
                'count' => count($getRatingCount),
                'rate' => $getRatingSum
            );
            $i++;
        }

        // Fetch parent category name by category id frmo url segment
        $parentCategoryName = Category::where('id', $parentCategoryId)->get()->toArray();
        // Fetch category name by category id frmo url segment
        $categoryName = Category::where('id', $categoryId)->get()->toArray();

        return view('pages.sub_category', compact('products', 'parentCategoryName', 'categoryName'));
    }

    /*public function secondCategory($parent, $category)
    {
        $categories = Category::where('parent', '!=', 0)->get();
        $c = null;

        foreach ($categories as $cat) {
            if (changeSpecialChar($cat->name) == $category && $cat->parentCategory && changeSpecialChar($cat->parentCategory->name) == $parent) {
                $c = $cat;
                dd($cat);
                break;
            }
        }

        if ($c) {
            return $this->subCategoryPage($c);
        }

        abort(404);
    }*/

    public function thirdCategory($parent, $second, $category){

	    $categories = Category::where('parent', '!=', 0)
	                                 ->get();

	    foreach ($categories as $cat) {
		    if (changeSpecialChar($cat->name) == $category) {
			    $category = $cat;
			    break;
		    }
	    }

	    if ($category) {
		    return $this->catalogPage($category);
	    }

	    abort(404);
    }

    public function getItemsSubCategory(Request $request) {

        $query = Item::query();

        $query->where('status', 1);

        if ($request->secondCategory && $request->secondCategory != '')
            $query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_third_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);

        // Sorting
        if ($request->sorting){
            if ($request->sorting == '1') {
                $query->orderBy('sorting');
                $query->orderBy('activated_at', 'desc');
            } else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        $items = $query->with('images', 'vendor', 'colors')->paginate(55);

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$'.sprintf('%0.2f', $item->price);

            foreach($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $item->colorsImages = $colorsImages;
            $item->video = ($item->video) ? asset($item->video) : null;

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;

            // Blocked Check
//			if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
//				$item->imagePath = asset('images/blocked.jpg');
//				$item->vendor->company_name = '';
//				$item->vendorUrl = '';
//				$item->style_no = '';
//				$item->price = '';
//				$item->available_on = '';
//				$item->colors->splice(0);
//			}
        }

//		return ['items' => [], 'pagination' => 3];

        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        return ['items' => $items->toArray(), 'pagination' => $paginationView];
    }

    public function getItemsCategory(Request $request) {

        $query = Item::query();

        $query->where('status', 1);

//		if ($request->secondCategory && $request->secondCategory != '')
//			$query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_parent_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);

        // Sorting
        if ($request->sorting){
            if ($request->sorting == '1') {
                $query->orderBy('sorting');
                $query->orderBy('activated_at', 'desc');
            } else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        $items = $query->with('images', 'vendor', 'colors')->paginate(55);

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();


//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];

            foreach($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');


            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$'.sprintf('%0.2f', $item->price);

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);


            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="'.$item->id.'"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;

            // Blocked Check
//			if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
//				$item->imagePath = asset('images/blocked.jpg');
//				$item->vendor->company_name = '';
//				$item->vendorUrl = '';
//				$item->style_no = '';
//				$item->price = '';
//				$item->available_on = '';
//				$item->colors->splice(0);
//			}
        }

//		return ['items' => [], 'pagination' => 3];

        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        return ['items' => $items->toArray(), 'pagination' => $paginationView];
    }

    public function subCategoryPage($category) 
    {
        $category->load('subCategories', 'parentCategory', 'lengths');

        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')->get();

        $bodySizes = BodySize::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $patterns = Pattern::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $styles = Style::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];
        /* $items = Item::where('status', 1)
            ->where('default_second_category', $category->id)
            ->orderBy('activated_at', 'desc')
            ->with('images', 'colors')
            ->paginate(54);
        */

        foreach($category->subCategories as &$cat) {
            $count = Item::where('status', 1)->where('default_third_category', $cat->id)->count();
            $cat->totalItem = $count;
        }

        foreach($vendors as &$vendor) {
            $count = Item::where('status', 1)
                ->where('default_second_category', $category->id)->count();
            $vendor->totalItem = $count;
        }

        // Wishlist
        $wishListItems = [];
        if( Auth::check() && Auth::user() && Auth::user()->id){
            $obj = new WishListItem();
            $wishListItems = $obj->getItemIds();
        }

//		dd($wishListItems);
//		dd('everything is ok');
        return view('pages.sub_category', compact('category', 'vendors', 'masterColors', 'masterFabrics',
            'items', 'wishListItems', 'bodySizes', 'patterns', 'styles'));
    }

	public function catalogPage($category) {
		$category->load('parentCategory');
		$parentCategory = $category->parentCategory;
		$parentCategory->load('subCategories', 'lengths', 'parentCategory');


		$patterns = Pattern::where('parent_category_id', $parentCategory->parentCategory->id)
		                   ->orderBy('name')
		                   ->get();

		$styles = Style::where('parent_category_id', $parentCategory->parentCategory->id)
		               ->orderBy('name')
		               ->get();

//		dd($parentCategory->subCategories);

		$masterColors = MasterColor::orderBy('name')->get();
		$masterFabrics = MasterFabric::orderBy('name')->get();

		$items = [];
		/*$items = Item::where('status', 1)
			->where('default_third_category', $category->id)
			->orderBy('activated_at', 'desc')
			->with('vendor', 'images', 'colors')
			->paginate(30);*/

		// Wishlist
//		$obj = new WishListItem();
//		$wishListItems = $obj->getItemIds();


		$wishListItems = [];
		if( Auth::check() && Auth::user() && Auth::user()->id){
			$obj = new WishListItem();
			$wishListItems = $obj->getItemIds();
		}

		return view('pages.catalog', compact('category', 'parentCategory', 'masterColors',
			'masterFabrics', 'items', 'wishListItems', 'patterns', 'styles'));
	}

	public function bestSellerPage(Request $request){

		// Best Selling Items
		$sql = "SELECT items.id, t.count 
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 60";


		$bestItems = DB::select($sql);

		$bestItemIds = [];
		foreach ($bestItems as $item)
			$bestItemIds[] = $item->id;



		$bestItems = [];
		if (sizeof($bestItemIds) > 0) {
			$bestItems = Item::whereIn('id', $bestItemIds)
			                 ->with('images', 'vendor', 'colors')
			                 ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
			                 ->get();
		}

//		dd($bestItems->toArray());
		$categories = Category::where('parent', '=', 0)->get();
        $activeItemIds = Item::where('status', 1)->pluck('id')->toArray();

		foreach($categories as &$category) {
			$category->count = ItemCategory::whereIn('item_id', $activeItemIds)
                ->where('default_parent_category', $category->id)
                ->distinct('item_id')
                ->count();;
		}

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

		return view('pages.best_seller', compact('bestItems', 'categories', 'wishListItems'));
	}

    public function searchPage(Request $request) {
        $input = $request->input();
        $limit = 12;
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : 'sorting';
        $order_by = isset($input['order_by']) ? $input['order_by'] : 'asc';

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {
            $items = Item::where('status', 1)
                ->where('min_qty', '>', 0)
                ->orderBy($sort_by, $order_by)
                ->where(function ($query) use ($request) {
                    $query->where('style_no', 'like', '%' . $request->s . '%')
                        ->orWhere('name', 'like', '%' . $request->s . '%')
                        ->orWhere('description', 'like', '%' . $request->s . '%');
                })
                ->with('images')->paginate(30);
        } else {
            $items = Item::where('status', 1)
                ->where('min_qty', '>', 0)
                ->orderBy($sort_by, $order_by)
                ->where(function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->s . '%')
                        ->orWhere('description', 'like', '%' . $request->s . '%');
                })
                ->with('images')->paginate(30);
        }

        $i = 0;
        foreach ( $items as $item ) {
            // Get rating count
            $getRatingCount = DB::table('product_reviews')->where('product_id', $item->id)->get()->toArray();

            // Get rating summation
            $getRatingSum = DB::table('product_reviews')->where('product_id', $item->id)->sum('rating');
            
            if ( count($getRatingCount) > 0 ) {
                $getRatingSum = ( $getRatingSum / count($getRatingCount) );   
            }
            else {
                $getRatingSum = 0;
            }

            $items[$i]['rating'] = array(
                'count' => count($getRatingCount),
                'rate' => $getRatingSum
            );
            $i++;
        }

        return view('pages.search', compact('items'));
    }
}
