<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\CouponType;
use App\Enumeration\OrderStatus;
use App\Enumeration\PageEnumeration;
use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\AdminShipMethod;
use App\Model\BuyerShippingAddress;
use App\Model\CartItem;
use App\Model\Country;
use App\Model\Coupon;
use App\Model\MetaBuyer;
use App\Model\MetaVendor;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\ShippingMethod;
use App\Model\State;
use App\Model\StoreCredit;
use App\Model\User;
use App\Model\Page;
use App\Model\VendorImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use CreditCard;
use Illuminate\Support\Facades\DB;
use Stripe\Stripe;
use Validator;
use Mail;
use PDF;
use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;

// PayPal
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\ShippingAddress;

// For Authorize Checkout
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class CheckoutController extends Controller
{
    public function create(Request $request) {
        $input = $request->input();
        if(!Auth::check()){
            if(isset($input['user'])){
                if(isset($input['user']['email']) && isset($input['user']['password']) && !empty($input['user']['email']) && !empty($input['user']['password'])){
                    $userCheck = User::where('email', $input['user']['email'])->get()->first();
                    if($userCheck != null){
                        return redirect()->back()->withErrors(['error' => 'Email address already taken']);
                    }

                    $meta = MetaBuyer::create([
                        'verified' => 1,
                        'active' => 1,
                        'user_id' => 0,
                        'company_name' => null,
                        'primary_customer_market' => null,
                        'seller_permit_number' => null,
                        'sell_online' => null,
                        'website' => null,
                        'attention' => null,
                        'billing_location' => null,
                        'billing_address' => null,
                        'billing_unit' => null,
                        'billing_city' => null,
                        'billing_state_id' => null,
                        'billing_state' => null,
                        'billing_zip' => null,
                        'billing_country_id' => null,
                        'billing_phone' => null,
                        'billing_fax' => null,
                        'billing_commercial' => 0,
                        'hear_about_us' => null,
                        'hear_about_us_other' => null,
                        'receive_offers' => null,
                        'ein_path' => null,
                        'sales1_path' => null,
                        'sales2_path' => null,
                    ]);
                    $user = User::create([
                        'first_name' => $input['billing']['first_name'],
                        'last_name' => $input['billing']['last_name'],
                        'email' => $input['user']['email'],
                        'password' => bcrypt($input['user']['password']),
                        'role' => Role::$BUYER,
                        'buyer_meta_id' => $meta->id,
                    ]);
                    BuyerShippingAddress::create([
                        'user_id' => $user->id,
                        'default' => 1,
                        'store_no' => null,
                        'location' => null,
                        'address' => null,
                        'unit' => null,
                        'city' => null,
                        'state_id' => null,
                        'state_text' => null,
                        'zip' => null,
                        'country_id' => null,
                        'phone' => null,
                        'fax' => null,
                        'commercial' => 0,
                    ]);
                    $meta->user_id = $user->id;
                    $meta->save();
                    Auth::login($user, true);

                    if(session('guestKey')[0] != null){
                        $guestId = session('guestKey')[0];
                        CartItem::where('user_id', $guestId)->update(['user_id' => Auth::user()->id]);
                    }

                } else {
                    return redirect()->back()->withErrors(['error' => 'Invalid sign up information']);
                }
            } else {
                return redirect()->back()->withErrors(['error' => 'Invalid sign up information']);
            }
        }

        $user = Auth::user();
        $user->load('buyer');

        // Cart Items
        $cartItems = CartItem::where('user_id', $user->id)->get();
        $subTotal = 0;
        $orderNumber = $this->generateRandomString(13);
        $invoice_number = $this->generateRandomString(13);
        if(count($cartItems) == 0){
            return redirect()->back()->withErrors(['error' => 'Invalid Cart']);
        }
        BuyerShippingAddress::where('user_id', $user->id)->update([
            'address' => $input['shipping']['address'],
            'unit' => $input['shipping']['unit'],
            'city' => $input['shipping']['city'],
            'state_id' => isset($input['shipping']['state_id']) ? $input['shipping']['state_id'] : null,
            'state_text' => isset($input['shipping']['state_text']) ? $input['shipping']['state_text'] : null,
            'zip' => isset($input['shipping']['zip']) ? $input['shipping']['zip'] : null,
            'country_id' => isset($input['shipping']['country_id']) ? $input['shipping']['country_id'] : null,
            'phone' => isset($input['shipping']['phone']) ? $input['shipping']['phone'] : null,
        ]);
        MetaBuyer::where('user_id', $user->id)->update([
            'billing_location' => null,
            'billing_address' => $input['billing']['address'],
            'billing_unit' => $input['billing']['unit'],
            'billing_city' => $input['billing']['city'],
            'billing_state' => isset($input['billing']['state']) ? $input['billing']['state'] : null,
            'billing_state_id' => isset($input['billing']['state_id']) ? $input['billing']['state_id'] : null,
//            'billing_state_text' => isset($input['billing']['state_text']) ? $input['billing']['state_text'] : null,
            'billing_zip' => isset($input['billing']['zip']) ? $input['billing']['zip'] : null,
//            'billing_country' => isset($input['billing']['country']) ? $input['billing']['country'] : null,
            'billing_country_id' => isset($input['billing']['country_id']) ? $input['billing']['country_id'] : null,
            'billing_phone' => isset($input['billing']['phone']) ? $input['billing']['phone'] : null
        ]);

        $order = Order::create([
            'status' => OrderStatus::$INIT,
            'user_id' => $user->id,
            'name' => $user->first_name.' '.$user->last_name,
            'company_name' => $user->buyer->company_name,
            'email' => $user->email,
            'billing_location' => null,
            'billing_address' => $input['billing']['address'],
            'billing_unit' => $input['billing']['unit'],
            'billing_city' => $input['billing']['city'],
            'billing_state' => isset($input['billing']['state']) ? $input['billing']['state'] : null,
            'billing_state_id' => isset($input['billing']['state_id']) ? $input['billing']['state_id'] : null,
            'billing_state_text' => isset($input['billing']['state_text']) ? $input['billing']['state_text'] : null,
            'billing_zip' => isset($input['billing']['zip']) ? $input['billing']['zip'] : null,
            'billing_country' => isset($input['billing']['country']) ? $input['billing']['country'] : null,
            'billing_country_id' => isset($input['billing']['country_id']) ? $input['billing']['country_id'] : null,
            'billing_phone' => isset($input['billing']['phone']) ? $input['billing']['phone'] : null,
            'shipping_method_id' => isset($input['shipping_method']) ? $input['shipping_method'] : 0,
            'shipping_location' => null,
            'shipping_address' => $input['shipping']['address'],
            'shipping_unit' => $input['shipping']['unit'],
            'shipping_city' => $input['shipping']['city'],
            'shipping_state' => isset($input['shipping']['state']) ? $input['shipping']['state'] : null,
            'shipping_state_id' => isset($input['shipping']['state_id']) ? $input['shipping']['state_id'] : null,
            'shipping_state_text' => isset($input['shipping']['state_text']) ? $input['shipping']['state_text'] : null,
            'shipping_zip' => isset($input['shipping']['zip']) ? $input['shipping']['zip'] : null,
            'shipping_country' => isset($input['shipping']['country']) ? $input['shipping']['country'] : null,
            'shipping_country_id' => isset($input['shipping']['country_id']) ? $input['shipping']['country_id'] : null,
            'shipping_phone' => isset($input['shipping']['phone']) ? $input['shipping']['phone'] : null,
            'payment_type' => isset($input['payment_type']) ? $input['payment_type'] : 'stripe',
            'card_number' => $input['card']['number'] != null ? encrypt($input['card']['number']) : encrypt(null),
            'card_full_name' => isset($input['card']['card_brand']) ? encrypt($input['card']['card_brand']) : encrypt(null),
            'card_expire' => encrypt($input['card']['exp_month'].'/'.$input['card']['exp_year']),
            'card_cvc' => isset($input['card']['cvc']) ? encrypt($input['card']['cvc']) : encrypt(null),
        ]);

        foreach ($cartItems as $cartItem) {
            OrderItem::create([
                'order_id' => $order->id,
                'item_id' => $cartItem->item_id,
                'style_no' => $cartItem->item->style_no,
                'color' => '',
                'size' => '',
                'item_per_pack' => 0,
                'pack' => '',
                'qty' => $cartItem->quantity,
                'total_qty' => $cartItem->quantity,
                'per_unit_price' => $cartItem->item->price,
                'amount' => $cartItem->quantity * $cartItem->item->price,
            ]);
            $subTotal = $subTotal + ($cartItem->quantity * $cartItem->item->price);
        }
        CartItem::where('user_id', $user->id)->delete();
        $shipping_cost = 0;
        $shipping_methods = AdminShipMethod::with('courier')->where('id', $input['shipping_method'])->get()->first();
        if ( $shipping_methods != null ) {
            $shipping_cost = $shipping_methods->fee;
        }

        $discount = 0;
        if ( session()->has('promo') ) {
            $promoType = session('promo')[0]['type'];
            $promo_amount = session('promo')[0]['amount'];
            $promo = 0;
            if ( $promoType == 1 ) { // Fixed amount will be deduct
                $promo = $promo_amount;
            }
            elseif ( $promoType == 2 ) { // Percentage amount will be deduct
                $promo = ( $subTotal * $promo_amount ) / 100;
            }
            else {
                $promo = $shipping_methods->fee;
            }
            $discount = $promo;
            $order->promo_code_id = session('promo')[0]['id'];
        }

        $order->order_number = $orderNumber;
        $order->invoice_number = $invoice_number;
        $order->subtotal = (string)$subTotal;
        $order->discount = (string)$discount;
        $order->shipping_cost = $shipping_cost;
        $order->total = (string)($subTotal + (float)$shipping_cost - $discount);
        $order->save();
        session()->forget('promo');

        if ( $input['payment_type'] == 'paypal' ) {
            $paypal_res = json_decode($input['paypal_res'], true);
            $order->paypal_payment_id = $paypal_res['id'];
            $order->paypal_pay_info = $input['paypal_res'];
            $order->paypal_payer_id = $paypal_res['payer']['payer_id'];
        } else {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET', 'sk_test_J25eP9avBbtgwKwcVYWPFc4H'));
            $stripe = \Stripe\Charge::create([
                "amount" => (int)($order->total*100),
                "currency" => "usd",
                "source" => $input['card']['stripeToken'], // obtained with Stripe.js
                "description" => "Charge for R3 All - Order Number ".$orderNumber
            ]);
            $stripe_payment_info = null;
            if ( $stripe ) {
                $stripe_payment_info = array(
                    'id' => $stripe->id,
                    'object' => $stripe->object,
                    'amount' => $stripe->amount,
                    'amount_refunded' => $stripe->amount_refunded,
                    'balance_transaction' => $stripe->balance_transaction,
                    'captured' => $stripe->captured,
                    'currency' => $stripe->currency,
                    'description' => $stripe->description,
                    'receipt_url' => $stripe->receipt_url,
                    'refunds' => $stripe->refunds,
                    'source' => $stripe->source,
                    'status' => $stripe->source,
                );
                $stripe_payment_info = json_encode($stripe_payment_info, true);
            }
            $order->stripe_payment_info = $stripe_payment_info;
            $order->save();
        }
        $order->status =  OrderStatus::$NEW_ORDER;
        $order->save();

        $pdfData = $this->getPdfData($order);
        try {
            // Send Mail to Buyer
            $order_time = Carbon::parse($order->created_at)->format('F d ,Y h:i:s A ');
            Mail::send('emails.buyer.order_confirmation', ['order' => $order, 'order_time' => $order_time], function ($message) use ($order, $pdfData) {
                $message->subject('Order Confirmed');
                $message->to($order->email, $order->name);
                $message->attachData($pdfData, $order->order_number . '.pdf');
            });

            // Send Mail to Vendor
            $user = User::where('role', Role::$EMPLOYEE)->first();

            Mail::send('emails.vendor.new_order', ['order' => $order], function ($message) use ($order, $pdfData, $user) {
                $message->subject('New Order - '.$order->order_number);
                $message->to($user->email, $user->first_name.' '.$user->last_name);
                $message->attachData($pdfData, $order->order_number.'.pdf');
            });
        } catch (\Exception $exception) {

        }
        return redirect()->route('checkout_complete' , ['order' => $order->id]);
    }

    public function index(Request $request) {
        $user = Auth::user();
        $user->load('buyer');

        // Check Orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $order = Order::where('id', $id)->first();

        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        if ($order->shipping_address_id == null)
            $address = BuyerShippingAddress::where('user_id', Auth::user()->id)->where('default', 1)->first();
        else
            $address = BuyerShippingAddress::where('id', $order->shipping_address_id)->first();

        $shippingAddresses = BuyerShippingAddress::where('user_id', Auth::user()->id)->get();

        return view('buyer.checkout.index', compact('user', 'countries', 'usStates', 'caStates',
            'order', 'address', 'shippingAddresses'))
            ->with('page_title', 'Checkout');
    }

    public function addressPost(Request $request) {
        // Check Orders
        $id = '';

        try {
            $id = decrypt($request->orders);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $shippingAddress = BuyerShippingAddress::where('id', $request->address_id)->with('state', 'country')->first();

        Order::where('id', $id)->update([
            'shipping_address_id' => $request->address_id,
            'shipping_location' => $shippingAddress->location,
            'shipping_address' => $shippingAddress->address,
            'shipping_city' => $shippingAddress->city,
            'shipping_state' => ($shippingAddress->state == null) ? $shippingAddress->state_text : $shippingAddress->state->name,
            'shipping_state_id' => $shippingAddress->state_id,
            'shipping_state_text' => $shippingAddress->state_text,
            'shipping_zip' => $shippingAddress->zipCode,
            'shipping_country' => $shippingAddress->country->name,
            'shipping_country_id' => $shippingAddress->country->id,
            'shipping_phone' => $shippingAddress->phone,
        ]);

        return redirect()->route('show_shipping_method', ['id' => $request->orders]);
    }

    public function shipping(Request $request) {
        // Check Orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $order = Order::where('id', $id)->first();

        $shipping_methods = AdminShipMethod::with('courier')->get();

	    //AuthorizeNet::authorizeCreditCard(10,true);

        return view('buyer.checkout.shipping', compact( 'order', 'shipping_methods'))->with('page_title', 'Checkout');
    }

    public function shippingPost(Request $request) {
        // Check orders
        $id = [];

        try {
            $id = decrypt($request->orderIds);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $request->validate([
            'shipping_method' => 'required'
        ]);


        $shippingMethod = AdminShipMethod::where('id', $request->shipping_method)->with('courier')->first();

        $order = Order::where('id', $id)->first();

        $order->shipping = $shippingMethod->name;
        $order->shipping_method_id = $shippingMethod->id;
        $order->shipping_cost = $shippingMethod->fee;
        $order->total = $order->total + $shippingMethod->fee;
        $order->save();

        return redirect()->route('show_payment', ['id' => $request->orderIds]);
    }

    public function payment(Request $request) {
        // Check orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $order = Order::where('id', $id)->first();

        // Decrypt
        $cardNumber = '';
        $cardFullName = '';
        $cardExpire = '';
        $cardCvc = '';

        try {
            $cardNumber = decrypt($order->card_number);
            $cardFullName = decrypt($order->card_full_name);
            $cardExpire = decrypt($order->card_expire);
            $cardCvc = decrypt($order->card_cvc);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;
        $order->card_full_name = $cardFullName;
        $order->card_expire = $cardExpire;
        $order->card_cvc = $cardCvc;

        return view('buyer.checkout.payment', compact('user', 'order'))->with('page_title', 'Checkout');
    }

    public function paymentPost(Request $request) {
        $validator = Validator::make($request->all(), [
            'number' => 'required|max:191|min:6',
            'name' => 'required|max:191',
            'expiry' => 'required|date_format:"m/y"',
            'cvc' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $validator->after(function ($validator) use($request) {
            // Card Number Check
            $card = CreditCard::validCreditCard($request->number);

            if (!$card['valid'])
                $validator->errors()->add('number', 'Invalid Card Number');

            // CVC Check
            $validCvc = CreditCard::validCvc($request->cvc, $card['type']);
            if (!$validCvc)
                $validator->errors()->add('cvc', 'Invalid CVC');

            // Expiry Check
            $tmp  = explode('/', $request->expiry);
            $validDate = CreditCard::validDate('20'.$tmp[1], $tmp[0]);
            if (!$validDate)
                $validator->errors()->add('expiry', 'Invalid Expiry');
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Check orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        Order::where('id', $id)->update([
            'card_number' => encrypt($request->number),
            'card_full_name' => encrypt($request->name),
            'card_expire' => encrypt($request->expiry),
            'card_cvc' => encrypt($request->cvc),
        ]);

//	    AuthorizeNet::authorizeCreditCard(10,true);

        return redirect()->route('show_checkout_review', ['id' => $request->id]);
    }

    public function review(Request $request) {
        // Check orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);


        $order = Order::where('id', $id)->with('user')->first();

        $temp = [];
        $cartItems = [];

        $cartObjs = CartItem::where('user_id', $order->user_id)
            ->with('item', 'color')
            ->get()
            ->sortBy(function ($useritem, $key) {
                return $useritem->item->vendor_meta_id;
            });

        foreach ($cartObjs as $obj) {
            $temp[$obj->item->id][] = $obj;
        }

        $itemCounter = 0;
        foreach ($temp as $itemId => $item) {
            $cartItems[$itemCounter] = $item;
            $itemCounter++;
        }

        $order->cartItems = $cartItems;
        $orders[] = $order;

        // Decrypt
        $cardNumber = '';
        $cardFullName = '';
        $cardExpire = '';
        $cardCvc = '';

        try {
            $cardNumber = decrypt($order->card_number);
            $cardNumber = str_repeat("*", (strlen($cardNumber) - 4)).substr($cardNumber,-4,4);
            $cardFullName = decrypt($order->card_full_name);
            $cardExpire = decrypt($order->card_expire);
            $cardCvc = decrypt($order->card_cvc);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;
        $order->card_full_name = $cardFullName;
        $order->card_expire = $cardExpire;
        $order->card_cvc = $cardCvc;

        return view('buyer.checkout.review', compact('order'))->with('page_title', 'Checkout');
    }

    public function complete(Order $order) {

        //return $order;

        return view('buyer.checkout.complete', compact('order'));
    }

    public function completeView(Request $request) {
        // Check orders
        $id = [];

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $order = Order::where('id', $id)->first();

        return view('buyer.checkout.complete', compact('order'));
    }

    public function addressSelect(Request $request) {
        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {
            $id = '';
        }

        Order::where('id', $id)->update(['shipping_address_id' => $request->shippingId]);
    }

    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getPdfData($order) {
        $allItems = [];
        $order->load('user', 'items');

        // Decrypt
        $cardNumber = '';
        $cardFullName = '';
        $cardExpire = '';
        $cardCvc = '';

        try {
            $cardNumber = decrypt($order->card_number);
            $cardFullName = decrypt($order->card_full_name);
            $cardExpire = decrypt($order->card_expire);
            $cardCvc = decrypt($order->card_cvc);
        } catch (DecryptException $e) {

        }

        $order->card_number = $cardNumber;
        $order->card_full_name = $cardFullName;
        $order->card_expire = $cardExpire;
        $order->card_cvc = $cardCvc;

        foreach($order->items as $item)
            $allItems[$item->item_id][] = $item;

        // Vendor Logo
        $logo_path = '';
        $black = DB::table('settings')->where('name', 'logo-black')->first();
        if ($black){
            $logo_path = asset($black->value);
        }

        $vendor = MetaVendor::where('id', 1)->first();
        $order->vendor = $vendor;

        $content = '';

        $page = Page::where('page_id', PageEnumeration::$RETURN_INFO)->first();
        if ($page)
            $content = $page->content;

        $data = [
            'all_items' => [$allItems],
            'orders' => [$order],
            'logo_paths' => [$logo_path],
            'page_content' => $content
        ];



        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('admin.dashboard.orders.pdf.with_image', $data);
        return $pdf->output();
    }

	/**
	 * @param Request $request
	 * Authorize and Capture Data From Vendor End.
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function authorizeOnly($order) {

		//1. Style No: N234, 2. Style No: N4234
//    	$orderId = $request->order;
//    	$order = Order::where('id', $orderId)->with('user', 'items')->first();


    	$orderDesc = '';
    	$oInc = 1;
    	foreach ($order->items as $oitem){
		    $orderDesc.= $oInc.'. Style No: '. $oitem->style_no .', ';
		    $oInc = $oInc + 1;
	    }

    	$invoiceId = $order->order_number;

//    	dd($order->toArray());
		$authorize_info = $order->authorize_info;

		$cardNumber = '';
		$cardFullName = '';
		$cardExpire = '';
		$cardCvc = '';

		try {
			$cardNumber = decrypt($order->card_number);
//			$cardNumber = str_repeat("*", (strlen($cardNumber) - 4)).substr($cardNumber,-4,4);
			$cardFullName = decrypt($order->card_full_name);
			$cardExpire = decrypt($order->card_expire);
			$cardCvc = decrypt($order->card_cvc);
		} catch (DecryptException $e) {

		}



		$fName =  $order->user->first_name;
		$lName =  $order->user->last_name;
		$b_address =  $order->billing_address;
		$b_city =  $order->billing_city;
		$b_state =  $order->billing_state;
		$b_zip =  $order->billing_zip;
		$b_country =  $order->billing_country;
		$user_id = $order->user->id;
		$user_email = $order->user->email;


		$s_address =  $order->shipping_address;
		$s_city =  $order->shipping_city;
		$s_state =  $order->shipping_state;
		$s_zip =  $order->shipping_zip;
		$s_country =  $order->shipping_country;



		$amount = $order->total;
		$expireData = explode('/', $cardExpire);
		$exYear = 2000 + intval($expireData[1]);
		$exMonth = $expireData[0];
		$expiry = $exYear.'-'.$exMonth;

//		dd($order->user->first_name);

		// Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(config('services.authorize.login'));
		$merchantAuthentication->setTransactionKey(config('services.authorize.key'));
		$refId = 'ref'.time();


		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($cardNumber);
		$creditCard->setExpirationDate($expiry);
		$creditCard->setCardCode($cardCvc);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);


		// Create order information
		$orderInfo = new AnetAPI\OrderType();
		$orderInfo->setInvoiceNumber($invoiceId);
		$orderInfo->setDescription($orderDesc);



		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($fName);
		$customerAddress->setLastName($lName);
//		$customerAddress->setCompany("Souveniropolis");
		$customerAddress->setAddress($b_address);
		$customerAddress->setCity($b_city);
		$customerAddress->setState($b_state);
		$customerAddress->setZip($b_zip);
		$customerAddress->setCountry($b_country);


		// Set the customer's Shipping Address
		$customerSAddress = new AnetAPI\CustomerAddressType();
		$customerSAddress->setFirstName($fName);
		$customerSAddress->setLastName($lName);
//		$customerAddress->setCompany("Souveniropolis");
		$customerSAddress->setAddress($s_address);
		$customerSAddress->setCity($s_city);
		$customerSAddress->setState($s_state);
		$customerSAddress->setZip($s_zip);
		$customerSAddress->setCountry($s_country);

		// Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId($user_id);
		$customerData->setEmail($user_email);

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
//		$transactionRequestType->setTransactionType("authOnlyTransaction");
		$transactionRequestType->setTransactionType("authOnlyTransaction");
//		$transactionRequestType->setShipping($amount);
		$transactionRequestType->setAmount($amount);
		$transactionRequestType->setOrder($orderInfo);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setShipTo($customerSAddress);
		$transactionRequestType->setCustomer($customerData);


		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

		$status_avs_hint = array("A" => 'Address (street) matches, ZIP code does not', 'B' => 'Address information not provided for AVS check',
		'E' => 'AVS error', 'G' => 'Non-U.S. card issuing bank', 'N' => 'No match on address (street) and ZIP code',
		'P' => 'AVS not applicable for this transaction', 'R' => 'Retry – System unavailable or timed out', 'S' => 'Service not supported by issuer',
		'U' => 'Address information is unavailable', 'W' => '9 digit ZIP code matches, address (street) does not', 'Y' => 'Address (street) and 5 digit ZIP code match',
		'Z' => '5 digit ZIP matches, Address (Street) does not'
		);

		$status_cvv_hint = array('M' => 'Successful Match', 'N' => 'Does NOT Match', 'P'=> 'Is NOT Processed',
			'S' => 'Should be on card, but is not indicated', 'U' => 'Issuer is not certified or has not provided encryption key'
			);



		$transactionInfo = (object) array();

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode()) {
//					pr($response);
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					$transactionInfo->status = 'Success';
					$transactionInfo->message = date('Y-m-d H:i:s') . " Authorized Only with ID: " . $tresponse->getTransId();
					$transactionInfo->transaction_code = $tresponse->getTransId();
					$transactionInfo->transaction_response_code = $tresponse->getResponseCode();
					$transactionInfo->message_code = $tresponse->getMessages()[0]->getCode();
					$transactionInfo->auth_code = $tresponse->getAuthCode();
					$transactionInfo->avs_code = $tresponse->getAvsResultCode();
					$transactionInfo->cvv_code = $tresponse->getCvvResultCode();
					$transactionInfo->desc = $tresponse->getMessages()[0]->getDescription();
					if($transactionInfo->avs_code){
						$transactionInfo->avs_message = $status_avs_hint[$transactionInfo->avs_code];
					}
					if($transactionInfo->cvv_code){
						$transactionInfo->cvv_message = $status_cvv_hint[$transactionInfo->cvv_code];
					}

				} else {
					$transactionInfo->status = 'Failed';
					$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
					$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
					$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
				}
				// Or, print errors if the API request wasn't successful
			} else {
				$transactionInfo->status = 'Failed';
				$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
				$tresponse = $response->getTransactionResponse();

				$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
				$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
			}
		} else {
			$transactionInfo->status = 'Failed';
			$transactionInfo->message = 'No Response(Failed)';
		}

		$transactionInfo->captured = false;

		$tInfo = json_encode($transactionInfo);
		return $tInfo;
	}

	public function authorizeAndCapture(Request $request) {

		//1. Style No: N234, 2. Style No: N4234
		$orderId = $request->order;
		$order = Order::where('id', $orderId)->with('user', 'items')->first();

		$orderDesc = '';
		$oInc = 1;
		foreach ($order->items as $oitem){
			$orderDesc.= $oInc.'. Style No: '. $oitem->style_no .', ';
			$oInc = $oInc + 1;
		}

		$invoiceId = $order->order_number;

//    	dd($order->toArray());
		$authorize_info = $order->authorize_info;

		$cardNumber = '';
		$cardFullName = '';
		$cardExpire = '';
		$cardCvc = '';

		try {
			$cardNumber = decrypt($order->card_number);
//			$cardNumber = str_repeat("*", (strlen($cardNumber) - 4)).substr($cardNumber,-4,4);
			$cardFullName = decrypt($order->card_full_name);
			$cardExpire = decrypt($order->card_expire);
			$cardCvc = decrypt($order->card_cvc);
		} catch (DecryptException $e) {

		}



		$fName =  $order->user->first_name;
		$lName =  $order->user->last_name;
		$b_address =  $order->billing_address;
		$b_city =  $order->billing_city;
		$b_state =  $order->billing_state;
		$b_zip =  $order->billing_zip;
		$b_country =  $order->billing_country;
		$user_id = $order->user->id;
		$user_email = $order->user->email;


		$s_address =  $order->shipping_address;
		$s_city =  $order->shipping_city;
		$s_state =  $order->shipping_state;
		$s_zip =  $order->shipping_zip;
		$s_country =  $order->shipping_country;



		$amount = $order->total;
		$expireData = explode('/', $cardExpire);
		$exYear = 2000 + intval($expireData[1]);
		$exMonth = $expireData[0];
		$expiry = $exYear.'-'.$exMonth;

//		dd($order->user->first_name);

		// Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(config('services.authorize.login'));
		$merchantAuthentication->setTransactionKey(config('services.authorize.key'));
		$refId = 'ref'.time();


		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($cardNumber);
		$creditCard->setExpirationDate($expiry);
		$creditCard->setCardCode($cardCvc);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);


		// Create order information
		$orderInfo = new AnetAPI\OrderType();
		$orderInfo->setInvoiceNumber($invoiceId);
		$orderInfo->setDescription($orderDesc);



		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($fName);
		$customerAddress->setLastName($lName);
//		$customerAddress->setCompany("Souveniropolis");
		$customerAddress->setAddress($b_address);
		$customerAddress->setCity($b_city);
		$customerAddress->setState($b_state);
		$customerAddress->setZip($b_zip);
		$customerAddress->setCountry($b_country);


		// Set the customer's Shipping Address
		$customerSAddress = new AnetAPI\CustomerAddressType();
		$customerSAddress->setFirstName($fName);
		$customerSAddress->setLastName($lName);
//		$customerAddress->setCompany("Souveniropolis");
		$customerSAddress->setAddress($s_address);
		$customerSAddress->setCity($s_city);
		$customerSAddress->setState($s_state);
		$customerSAddress->setZip($s_zip);
		$customerSAddress->setCountry($s_country);

		// Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId($user_id);
		$customerData->setEmail($user_email);

		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
//		$transactionRequestType->setTransactionType("authOnlyTransaction");
		$transactionRequestType->setTransactionType("authCaptureTransaction");
//		$transactionRequestType->setShipping($amount);
		$transactionRequestType->setAmount($amount);
		$transactionRequestType->setOrder($orderInfo);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		$transactionRequestType->setShipTo($customerSAddress);
		$transactionRequestType->setCustomer($customerData);


		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

		$status_avs_hint = array("A" => 'Address (street) matches, ZIP code does not', 'B' => 'Address information not provided for AVS check',
		                         'E' => 'AVS error', 'G' => 'Non-U.S. card issuing bank', 'N' => 'No match on address (street) and ZIP code',
		                         'P' => 'AVS not applicable for this transaction', 'R' => 'Retry – System unavailable or timed out', 'S' => 'Service not supported by issuer',
		                         'U' => 'Address information is unavailable', 'W' => '9 digit ZIP code matches, address (street) does not', 'Y' => 'Address (street) and 5 digit ZIP code match',
		                         'Z' => '5 digit ZIP matches, Address (Street) does not'
		);

		$status_cvv_hint = array('M' => 'Successful Match', 'N' => 'Does NOT Match', 'P'=> 'Is NOT Processed',
		                         'S' => 'Should be on card, but is not indicated', 'U' => 'Issuer is not certified or has not provided encryption key'
		);



		$transactionInfo = (object) array();

		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode()) {
//					pr($response);
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					$transactionInfo->status = 'Success';
					$transactionInfo->message = date('Y-m-d H:i:s') . " Authorized Only with ID: " . $tresponse->getTransId();
					$transactionInfo->transaction_code = $tresponse->getTransId();
					$transactionInfo->transaction_response_code = $tresponse->getResponseCode();
					$transactionInfo->message_code = $tresponse->getMessages()[0]->getCode();
					$transactionInfo->auth_code = $tresponse->getAuthCode();
					$transactionInfo->avs_code = $tresponse->getAvsResultCode();
					$transactionInfo->cvv_code = $tresponse->getCvvResultCode();
					$transactionInfo->desc = $tresponse->getMessages()[0]->getDescription();
					if($transactionInfo->avs_code){
						$transactionInfo->avs_message = $status_avs_hint[$transactionInfo->avs_code];
					}
					if($transactionInfo->cvv_code){
						$transactionInfo->cvv_message = $status_cvv_hint[$transactionInfo->cvv_code];
					}

				} else {
					$transactionInfo->status = 'Failed';
					$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
					$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
					$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
				}
				// Or, print errors if the API request wasn't successful
			} else {
				$transactionInfo->status = 'Failed';
				$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
				$tresponse = $response->getTransactionResponse();

				$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
				$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
			}
		} else {
			$transactionInfo->status = 'Failed';
			$transactionInfo->message = 'No Response(Failed)';
		}

		$tInfo = json_encode($transactionInfo);

		if(!(isset($authorize_info))){
			DB::table('orders')->where('id', $orderId)->update(['authorize_info' => $tInfo]);
		}
//		 DB::table('orders')->where('id', $orderId)->update(['authorize_info' => $tInfo]);

		$redirectUrl = route('admin_order_details', ['order' => $orderId]);
		return response()->json(['success' => true, 'url' => $redirectUrl, 'transaction' => $transactionInfo]);
	}

	public function captureAuthorizedAmount(Request $request) {

		//1. Style No: N234, 2. Style No: N4234
		$orderId = $request->order;
		$order = Order::where('id', $orderId)->with('user', 'items')->first();

		$orderDesc = '';
		$transactionid = '';
		$oInc = 1;
		foreach ($order->items as $oitem){
			$orderDesc.= $oInc.'. Style No: '. $oitem->style_no .', ';
			$oInc = $oInc + 1;
		}

		$invoiceId = $order->order_number;

		$authorize_info = json_decode($order->authorize_info, true);
		$transactionid = $authorize_info['transaction_code'];



		$amount = $order->total;

		$cardNumber = '';
		$cardFullName = '';
		$cardExpire = '';
		$cardCvc = '';

		try {
			$cardNumber = decrypt($order->card_number);
//			$cardNumber = str_repeat("*", (strlen($cardNumber) - 4)).substr($cardNumber,-4,4);
			$cardFullName = decrypt($order->card_full_name);
			$cardExpire = decrypt($order->card_expire);
			$cardCvc = decrypt($order->card_cvc);
		} catch (DecryptException $e) {

		}



		$amount = $order->total;
		$expireData = explode('/', $cardExpire);
		$exYear = 2000 + intval($expireData[1]);
		$exMonth = $expireData[0];
		$expiry = $exYear.'-'.$exMonth;

//		dd($order->user->first_name);

		// Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(config('services.authorize.login'));
		$merchantAuthentication->setTransactionKey(config('services.authorize.key'));
		$refId = 'ref'.time();


		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("priorAuthCaptureTransaction");
		$transactionRequestType->setAmount($amount);
		$transactionRequestType->setRefTransId($transactionid);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransactionRequest( $transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);

		$transactionInfo = (object) array();


		$status_avs_hint = array("A" => 'Address (street) matches, ZIP code does not', 'B' => 'Address information not provided for AVS check',
		                         'E' => 'AVS error', 'G' => 'Non-U.S. card issuing bank', 'N' => 'No match on address (street) and ZIP code',
		                         'P' => 'AVS not applicable for this transaction', 'R' => 'Retry – System unavailable or timed out', 'S' => 'Service not supported by issuer',
		                         'U' => 'Address information is unavailable', 'W' => '9 digit ZIP code matches, address (street) does not', 'Y' => 'Address (street) and 5 digit ZIP code match',
		                         'Z' => '5 digit ZIP matches, Address (Street) does not'
		);

		$status_cvv_hint = array('M' => 'Successful Match', 'N' => 'Does NOT Match', 'P'=> 'Is NOT Processed',
		                         'S' => 'Should be on card, but is not indicated', 'U' => 'Issuer is not certified or has not provided encryption key'
		);

		if ($response != null) {

//			dd($response);

			// Check to see if the API request was successfully received and acted upon
			// $response->getMessages()->getResultCode() == "Ok"
			if ($response->getMessages()->getResultCode()== "Ok" ) {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
					$transactionInfo->status = 'Success';
					$transactionInfo->message = date('Y-m-d H:i:s') . " Authorize and Captured with ID: " . $tresponse->getTransId();
					$transactionInfo->transaction_code = $tresponse->getTransId();
					$transactionInfo->transaction_response_code = $tresponse->getResponseCode();
					$transactionInfo->message_code = $tresponse->getMessages()[0]->getCode();
					$transactionInfo->auth_code = $tresponse->getAuthCode();
					$transactionInfo->avs_code = $tresponse->getAvsResultCode();
					$transactionInfo->cvv_code = $tresponse->getCvvResultCode();
					$transactionInfo->desc = $tresponse->getMessages()[0]->getDescription();
					$transactionInfo->avs_message = $status_avs_hint[$transactionInfo->avs_code];
					$transactionInfo->cvv_message = $status_cvv_hint[$transactionInfo->cvv_code];
					$transactionInfo->captured = true;
				} else {
					$transactionInfo->status = 'Failed';
					$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
					$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
					$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
				}
//				dd($transactionInfo);
				// Or, print errors if the API request wasn't successful
			} else {
				$transactionInfo->status = 'Failed';
				$transactionInfo->message = date('Y-m-d H:i:s') . ' Auth Failed';
				$tresponse = $response->getTransactionResponse();

				$transactionInfo->error_code = $tresponse->getErrors()[0]->getErrorCode();
				$transactionInfo->error_message = $tresponse->getErrors()[0]->getErrorText();
//				dd($transactionInfo);
			}
		} else {
//			dd($transactionInfo);
			$transactionInfo->status = 'Failed';
			$transactionInfo->message = 'No Response(Failed)';
		}

		$tInfo = json_encode($transactionInfo);

		if(!(isset($authorize_info))){
//			DB::table('orders')->where('id', $orderId)->update(['authorize_info' => $tInfo]);
		}
		 DB::table('orders')->where('id', $orderId)->update(['authorize_info' => $tInfo]);

		$redirectUrl = route('admin_order_details', ['order' => $orderId]);
		return response()->json(['success' => true, 'url' => $redirectUrl, 'transaction' => $transactionInfo]);
	}

    // public function applyCoupon(Request $request) {
    //     $order = Order::where('id', $request->id)->where('user_id', Auth::user()->id)->first();

    //     if (!$order)
    //         return response()->json(['success' => false, 'message' => 'Invalid Order.']);

    //     $coupon = Coupon::where('vendor_meta_id', $order->vendor_meta_id)
    //         ->where('name', $request->coupon)
    //         ->first();

    //     if (!$coupon)
    //         return response()->json(['success' => false, 'message' => 'Invalid Coupon.']);

    //     if ($coupon->multiple_use == 0) {
    //         $previous = Order::where('user_id', Auth::user()->id)
    //             ->where('status', '!=', OrderStatus::$INIT)
    //             ->where('vendor_meta_id', $coupon->vendor_meta_id)
    //             ->where('coupon', $coupon->name)
    //             ->first();

    //         if ($previous)
    //             return response()->json(['success' => false, 'message' => 'Already used this coupon.']);
    //     }

    //     $subTotal = $order->subtotal;
    //     $discount = 0;

    //     if ($coupon->type == CouponType::$FIXED_PRICE)
    //         $discount = $coupon->amount;
    //     else if ($coupon->type == CouponType::$PERCENTAGE){
    //         $discount = ($coupon->amount / 100) * $subTotal;
    //     } else if ($coupon->type == CouponType::$FREE_SHIPPING){
    //         $discount = 0;
    //     }

    //     if ($discount > $subTotal)
    //         $discount = $subTotal;

    //     $order->discount = $discount;
    //     $order->total = $subTotal - $discount;
    //     $order->coupon = $coupon->name;
    //     $order->coupon_type = $coupon->type;
    //     $order->coupon_amount = $coupon->amount;
    //     $order->coupon_description = $coupon->description;
    //     $order->save();

    //     return response()->json(['success' => true, 'message' => 'Success.']);
    // }

    public function singlePageCheckout(Request $request) 
    {
        $user = null;
        $sessionUser = null;
        if (Auth::check() && Auth::user()->role == Role::$BUYER) {
            $sessionUser = Auth::user()->id;
            $user = Auth::user();
        } else {
            $sessionUser = session('guestKey')[0];
        }

        $cartItems = CartItem::where('user_id', $sessionUser)
            ->with('item', 'color')
            ->get()->toArray();
        if(count($cartItems) == 0){
            return redirect()->route('show_cart');
        }

        $subTotal = 0;
        $promo = 0;
        $giftCard = 0;
        $total = 0;
        foreach ($cartItems as $cart){
            $eachPrice = (int)$cart['quantity'] * (float)$cart['item']['price'];
            $subTotal = $subTotal + $eachPrice;
        }

        $promoType = [];
        if ( session()->has('promo')) {
            $promoType = session('promo')[0]['type'];
            $promo_amount = session('promo')[0]['amount'];
            if ( $promoType == 1 ) { // Fixed amount will be deduct
                $promo = $promo_amount;
            }
            elseif ( $promoType == 2 ) { // Percentage amount will be deduct
                $promo = ( $subTotal * $promo_amount ) / 100;
            }
        }

        $total = $subTotal-$promo-$giftCard;

        $shippingAddresses = BuyerShippingAddress::where('user_id', $sessionUser)->with('state', 'country')->get()->first();
        $shipping_methods = AdminShipMethod::with('courier')->get()->toArray();
        $country = Country::get()->toArray();

//        dd($shipping_methods);

        return view('buyer.checkout.single', compact('shippingAddresses', 'promoType', 'user', 'shipping_methods', 'cartItems','cartItems', 'total', 'subTotal','promo','giftCard','country'));
        
        //return view('buyer.checkout.single');
    }

    public function singlePageCheckoutPost(Request $request) {
	    $rules = [
            'address_id' => 'required',
            'paymentMethod' => 'required|integer|min:1|max:3',
            'shipping_method' => 'required',
        ];

	    if ($request->paymentMethod == '2') {
            $rules['number'] = 'required|max:191|min:6';
            $rules['name'] = 'required|max:191';
            $rules['expiry'] = 'required|date_format:"m/y"';
            $rules['cvc'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->paymentMethod == '2') {
            $validator->after(function ($validator) use($request) {
                // Card Number Check
                $card = CreditCard::validCreditCard($request->number);

                if (!$card['valid'])
                    $validator->errors()->add('number', 'Invalid Card Number');

                // CVC Check
                $validCvc = CreditCard::validCvc($request->cvc, $card['type']);
                if (!$validCvc)
                    $validator->errors()->add('cvc', 'Invalid CVC');

                // Expiry Check
                $tmp  = explode('/', $request->expiry);
                $validDate = CreditCard::validDate('20'.$tmp[1], $tmp[0]);
                if (!$validDate)
                    $validator->errors()->add('expiry', 'Invalid Expiry');
            });

	            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        // Check Orders
        $id = '';

        try {
            $id = decrypt($request->id);
        } catch (DecryptException $e) {

        }

        if ($id == '')
            abort(404);

        $order = Order::where('id', $id)->where('status', OrderStatus::$INIT)->first();

        if (!$order)
            abort(404);

        $shipmentMethod = AdminShipMethod::where('id', $request->shipping_method)->first();
        $shippingAddress = BuyerShippingAddress::where('id', $request->address_id)->with('state', 'country')->first();

        if ($shipmentMethod->fee === null)
            $shipmentMethod->fee = 0;

        if ($request->paymentMethod != '3') {
            $preOrder = Order::where('order_number', 'like', 'CQ%')
                ->where('order_number', 'not like', '%BO%')
                ->orderBy('created_at', 'desc')
                ->first();
            $orderNumber = "CQ10001";

            if ($preOrder) {
                $tmp = (int) substr($preOrder->order_number, 2);
                $orderNumber = "CQ".($tmp+1);
            }

            $order->status = OrderStatus::$NEW_ORDER;
            $order->order_number = $orderNumber;
        }

        $order->user_id = Auth::user()->id;
        $order->email = Auth::user()->email;
        $order->company_name = Auth::user()->buyer->company_name;
        $order->shipping_method_id = $shipmentMethod->id;
        $order->shipping = $shipmentMethod->name;
        $order->can_call = $request->can_call;

        $order->shipping_address_id = $request->address_id;
        $order->shipping_location = $shippingAddress->location;
        $order->shipping_address = $shippingAddress->address;
        $order->shipping_unit = $shippingAddress->unit;
        $order->shipping_city = $shippingAddress->city;
        $order->shipping_state = ($shippingAddress->state == null) ? $shippingAddress->state_text : $shippingAddress->state->code;
        $order->shipping_state_id = $shippingAddress->state_id;
        $order->shipping_state_text = $shippingAddress->state_text;
        $order->shipping_zip = $shippingAddress->zip;
        $order->shipping_country = $shippingAddress->country->name;
        $order->shipping_country_id = $shippingAddress->country->id;
        $order->shipping_phone = $shippingAddress->phone;

	    $order->shipping_cost = $shipmentMethod->fee;
	    $order->total = $order->subtotal + $shipmentMethod->fee - $order->store_credit;

        if ($request->paymentMethod == '2') {
            $order->card_number = encrypt($request->number);
            $order->card_full_name = encrypt($request->name);
            $order->card_expire = encrypt($request->expiry);
            $order->card_cvc = encrypt($request->cvc);

            $order->payment_type = 'Credit Card';
            $card = CreditCard::validCreditCard($request->number);
	        $order->payment_type = $card['type'];

//	        $authorize = $this->authorizeOnly($order);

//	        $order->authorize_info = $authorize;

        } else if ($request->paymentMethod == '1') {
            $order->payment_type = 'Wire Transfer';
        } else if ($request->paymentMethod == '3') {
            $order->payment_type = 'PayPal';
        }

        $order->note = $request->order_note;
        $order->save();

        $user = Auth::user();
        $user->increment('order_count');


        if ($request->paymentMethod == '3') {
            $paypal_conf = \Config::get('paypal');
            $this->_api_context = new ApiContext(new OAuthTokenCredential(
                    $paypal_conf['client_id'],
                    $paypal_conf['secret'])
            );
            $this->_api_context->setConfig($paypal_conf['settings']);

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $items = [];
            $item_list = new ItemList();

            foreach ($order->items as $i) {
                $item = new Item();
                $item->setName($i->style_no.' - '.$i->color)
                    ->setCurrency('USD')
                    ->setQuantity($i->total_qty)
                    ->setPrice($i->per_unit_price);

                $items[] = $item;
            }

            $itemTmp = new \PayPal\Api\Item();
            $itemTmp->setName('Store Credit')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice('-'.number_format($order->store_credit, 2, '.', ''));

            $items[] = $itemTmp;

            $item_list->setItems($items);

            // Shipping Address
            /*$shipping_address = new \PayPal\Api\ShippingAddress();

            $shipping_address->setCity($shippingAddress->city);
            $shipping_address->setCountryCode($shippingAddress->country->code);
            $shipping_address->setPostalCode($shippingAddress->zip);
            $shipping_address->setLine1($shippingAddress->address);
            $shipping_address->setState(($shippingAddress->state == null) ? $shippingAddress->state_text : $shippingAddress->state->name);
            $shipping_address->setRecipientName($order->name);

            $item_list->setShippingAddress($shipping_address);*/

            $details = new Details();
            $details->setShipping($order->shipping_cost)
                ->setTax(0)
                ->setSubtotal($order->subtotal - $order->store_credit);


            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($order->subtotal + $order->shipping_cost - $order->store_credit)
                ->setDetails($details);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('https://www.fameaccoessories.com')
                ->setInvoiceNumber($order->order_number);

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(route('checkout_complete', ['id' => $request->id]))
                            ->setCancelUrl(route('show_cart'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PPConnectionException $ex) {
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::to('/');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::to('/');
                }
            }
            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::to('/');
        }

        CartItem::where([])->delete();

        $pdfData = $this->getPdfData($order);


        try {
            // Send Mail to Buyer
            Mail::send('emails.buyer.order_confirmation', ['order' => $order], function ($message) use ($order, $pdfData) {
                $message->subject('Order Confirmed');
                $message->to($order->email, $order->name);
                $message->attachData($pdfData, $order->order_number . '.pdf');
            });

            // Send Mail to Vendor
            $user = User::where('role', Role::$EMPLOYEE)->first();

            Mail::send('emails.vendor.new_order', ['order' => $order], function ($message) use ($order, $pdfData, $user) {
                $message->subject('New Order - '.$order->order_number);
                $message->to($user->email, $user->first_name.' '.$user->last_name);
                $message->attachData($pdfData, $order->order_number.'.pdf');
            });
        } catch (\Exception $exception) {

        }

        return view('buyer.checkout.complete', compact('order'));
    }

    //Promo code apply buyer (saif added)
    // public function applyCoupon(Request $request) 
    // {        
    //     $order = Order::where('id', $request->id)->where('user_id', Auth::user()->id)->first();
    //     dd($order);
    //     if (!$order)
    //         return response()->json(['success' => false, 'message' => 'Invalid Order.']);

    //     $coupon = Coupon::where('name', trim($request->coupon))->first();

    //     if (!$coupon)
    //         return response()->json(['success' => false, 'message' => 'Invalid Coupon.']);

    //     if ($coupon->multiple_use == 0) {
    //         $previous = Order::where('user_id', Auth::user()->id)
    //             ->where('status', '!=', OrderStatus::$INIT)
    //             //->where('vendor_meta_id', $coupon->vendor_meta_id)
    //             ->where('coupon', $coupon->name)
    //             ->first();

    //         if ($previous)
    //             return response()->json(['success' => false, 'message' => 'Already used this coupon.']);
    //     }

    //     $subTotal = $order->subtotal;
    //     $discount = 0;

    //     if ($coupon->type == CouponType::$FIXED_PRICE)
    //         $discount = $coupon->amount;
    //     else if ($coupon->type == CouponType::$PERCENTAGE){
    //         $discount = ($coupon->amount / 100) * $subTotal;
    //     } else if ($coupon->type == CouponType::$FREE_SHIPPING){
    //         $discount = 0;
    //     }

    //     if ($discount > $subTotal)
    //         $discount = $subTotal;

    //     $order->discount = $discount;
    //     $order->total = $subTotal - $discount;
    //     $order->coupon = $coupon->name;
    //     $order->coupon_type = $coupon->type;
    //     $order->coupon_amount = $coupon->amount;
    //     $order->coupon_description = $coupon->description;
    //     $order->save();

    //     // return response()->json(['success' => true, 'message' => 'Success.']);
    //     return redirect()->back()->with('message', 'Coupon applied!');
    // }
}
