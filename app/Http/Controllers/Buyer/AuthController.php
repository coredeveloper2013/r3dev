<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\Role;
use App\Model\BuyerShippingAddress;
use App\Model\CartItem;
use App\Model\Country;
use App\Model\LoginHistory;
use App\Model\MetaBuyer;
use App\Model\State;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Uuid;
use Carbon\Carbon;
use Mail;

class AuthController extends Controller
{
    public function mergeGuestToAuth(){

        if(session('guestKey')[0] != null){
            $guestId = session('guestKey')[0];
            CartItem::where('user_id', $guestId)->update(['user_id' => Auth::user()->id]);
        }
        return true;

    }

    public function register() {
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        return view('buyer.auth.register', compact('countries', 'usStates', 'caStates'))->with('page_title', 'Buyer Register');
    }

    public function registerPost(Request $request) {


        if(isset($request->provider_id) && $request->provider_id != null && $request->provider_id != ''){
            $user = User::where('provider_id', $request->provider_id)
                ->where('social', 'facebook')
                ->where('role', Role::$BUYER)
                ->with('buyer')->first();

            if($user != null){
                Auth::login($user, true);
                $this->mergeGuestToAuth();
                return redirect()->route('buyer_show_overview');
            }
            else {
                $request->validate([
                    'first_name' => 'required|max:50',
                    'last_name' => 'required|max:50',
                    'email' => 'required|email|unique:users',
                    'provider_id' => 'required',
                ]);
                $meta = MetaBuyer::create([
                    'verified' => 1,
                    'active' => 1,
                    'user_id' => 0,
                    'company_name' => null,
                    'primary_customer_market' => null,
                    'seller_permit_number' => null,
                    'sell_online' => null,
                    'website' => null,
                    'attention' => null,
                    'billing_location' => null,
                    'billing_address' => null,
                    'billing_unit' => null,
                    'billing_city' => null,
                    'billing_state_id' => null,
                    'billing_state' => null,
                    'billing_zip' => null,
                    'billing_country_id' => null,
                    'billing_phone' => null,
                    'billing_fax' => null,
                    'billing_commercial' => 0,
                    'hear_about_us' => null,
                    'hear_about_us_other' => null,
                    'receive_offers' => null,
                    'ein_path' => null,
                    'sales1_path' => null,
                    'sales2_path' => null,
                ]);
                $user = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => null,
                    'role' => Role::$BUYER,
                    'buyer_meta_id' => $meta->id,
                    'provider_id' => $request->provider_id,
                    'social' => 'facebook',
                ]);
                BuyerShippingAddress::create([
                    'user_id' => $user->id,
                    'default' => 1,
                    'store_no' => null,
                    'location' => null,
                    'address' => null,
                    'unit' => null,
                    'city' => null,
                    'state_id' => null,
                    'state_text' => null,
                    'zip' => null,
                    'country_id' => null,
                    'phone' => null,
                    'fax' => null,
                    'commercial' => 0,
                ]);

                $meta->user_id = $user->id;
                $meta->save();
                Auth::login($user, true);
                $this->mergeGuestToAuth();
                return redirect()->route('buyer_show_overview');
            }

        } else {
            $request->validate([
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed|min:6',
            ]);
            $meta = MetaBuyer::create([
                'verified' => 1,
                'active' => 1,
                'user_id' => 0,
                'company_name' => null,
                'primary_customer_market' => null,
                'seller_permit_number' => null,
                'sell_online' => null,
                'website' => null,
                'attention' => null,
                'billing_location' => null,
                'billing_address' => null,
                'billing_unit' => null,
                'billing_city' => null,
                'billing_state_id' => null,
                'billing_state' => null,
                'billing_zip' => null,
                'billing_country_id' => null,
                'billing_phone' => null,
                'billing_fax' => null,
                'billing_commercial' => 0,
                'hear_about_us' => null,
                'hear_about_us_other' => null,
                'receive_offers' => null,
                'ein_path' => null,
                'sales1_path' => null,
                'sales2_path' => null,
            ]);
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => Role::$BUYER,
                'buyer_meta_id' => $meta->id,
            ]);
            BuyerShippingAddress::create([
                'user_id' => $user->id,
                'default' => 1,
                'store_no' => null,
                'location' => null,
                'address' => null,
                'unit' => null,
                'city' => null,
                'state_id' => null,
                'state_text' => null,
                'zip' => null,
                'country_id' => null,
                'phone' => null,
                'fax' => null,
                'commercial' => 0,
            ]);
            $meta->user_id = $user->id;
            $meta->save();

            try {
                // Send Mail to Buyer

                Mail::send('emails.buyer.registration_complete',['user' => $user], function ($message) use ($request) {
                    $message->subject('Registration Complete');
                    $message->to($request->email, $request->firstName.' '.$request->lastName);
                });
                // Send Mail to Vendor
                $adminEmail = User::where('role', Role::$EMPLOYEE)->first();
                Mail::send('emails.buyer.registration_complete',['user' => $user], function ($message) use ($request,$adminEmail) {
                    $message->subject('Registration Complete');
                    $message->to($adminEmail->email);
                });
            } catch (\Exception $exception) {

            }
            return redirect()->route('buyer_register_complete');
        }

    }

    public function registerComplete() {
        return view('buyer.auth.complete');
    }

    public function login() {
        return view('buyer.auth.login')->with('page_title', 'Login / Register Account');
    }


    public function forgot_password_buyer() {
        return view('buyer.auth.forgot_password_buyer')->with('page_title', 'Password Recovery');
    }

    public function send_password_recover_email(Request $request){
        $user = User::where('email', $request->email)
            ->where('role', Role::$BUYER)
            ->with('buyer')->first();

        if (!$user)
            return redirect()->route('forgot_password_buyer')->with('message', 'Email not found.')->withInput();
        else
        {

            try {
                // Send Mail to Buyer

                $token = Uuid::generate()->string;

                $user->reset_token = $token;

                $user->save();
                $data = [
                    'email' => $user->email,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'token' => $token
                ];

                Mail::send('emails.buyer.password_recovery',['token' => $token], function ($message) use ($data) {
                    $message->subject('Password Recovery');
                    $message->to($data['email'], $data['first_name'].' '.$data['last_name'] , $data['token']);
                });
                // Send Mail to Vendor

            } catch (\Exception $exception) {

                //dd($exception);

            }

            return redirect()->route('buyer_login')->with('message', 'Passowrd email was sent')->withInput();

        }
    }

    public function reset_password_buyer($token){

        $user = User::where('reset_token', $token)
            ->where('role', Role::$BUYER)
            ->with('buyer')->first();

        if(!isset($user->email)){
            return redirect()->route('forgot_password_buyer')->with('message', 'Invalid Token')->withInput();
        }


        return view('buyer.auth.reset_password_buyer' , compact('user' , 'token'))->with('page_title', 'Set New Passowrd');

    }

    public function reset_password_buyer_now($token){
        $user = User::where('reset_token', $token)
            ->where('role', Role::$BUYER)
            ->with('buyer')->first();

        if(!isset($user->email)){
            return redirect()->route('forgot_password_buyer')->with('message', 'Invalid Token')->withInput();
        }



        request()->validate([
            'password' => 'required|min:6',
        ]);

        if(request('password') != request('confirm_password')){
            return back()->with('message', 'Password mismatch')->withInput();
        }

        $user->password = Hash::make(request('password'));
        $user->reset_token = '';
        $user->save();

        return redirect()->route('buyer_login')->with('message', 'Passowrd reset was successfull')->withInput();


    }

    public function loginPost(Request $request) {
        $user = User::where('email', $request->email)
            ->where('role', Role::$BUYER)
            ->with('buyer')->first();

        if (!$user)
            return redirect()->route('buyer_login')->with('message', 'Email not found.')->withInput();

        if ($user->buyer->verified == 0)
            return redirect()->back()->with('message', 'Buyer not verified.')->withInput();

        if ($user->buyer->active == 0)
            return redirect()->back()->with('message', 'Buyer not active.')->withInput();

        if ($user->buyer->block == 1)
            return redirect()->back()->with('message', 'Buyer is blocked.')->withInput();

        if (Hash::check($request->password, $user->password)) {
            if ($request->remember_me)
                Auth::login($user, true);
            else
                Auth::login($user);

            $user->last_login = Carbon::now()->toDateTimeString();
            //$user->increment('login_count');
            $user->save();

            LoginHistory::create([
                'user_id' => $user->id,
                'ip' => $request->ip(),
            ]);
            $this->mergeGuestToAuth();

            return redirect()->route('buyer_show_overview');
        }

        return redirect()->route('buyer_login')->with('message', 'Invalid Password.')->withInput();
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('home');
    }

    public function resetPassword() {
        return view('buyer.auth.reset_password');
    }

    public function resetPasswordPost(Request $request) {
        $user = User::where('role', Role::$BUYER)->where('email', $request->email)->first();

        if (!$user)
            return redirect()->back()->with('message', 'Email Not Found.')->withInput();

        $token = Uuid::generate()->string;

        $user->reset_token = $token;
        $user->save();

        Mail::send('emails.buyer.password_reset', ['token' => $token], function ($message) use ($user) {
            $message->subject('Reset Password');
            $message->to($user->email, $user->first_name.' '.$user->last_name);
        });

        return redirect()->back()->with('message', 'Email has sent with reset password link.');
    }

    public function newPassword(Request $request) {
        $data['profile_page'] = 'myInformation';
            return view('buyer.auth.new_password' , $data);
    }

    public function newPasswordPost(Request $request) {
        $request->validate([
            'password' => 'required|min:6|confirmed'
        ]);

        $user = User::where('role', Role::$BUYER)->where('reset_token', $request->token)->first();

        if (!$user)
            abort(404);

        $user->password = Hash::make($request->password);
        $user->reset_token = null;
        $user->save();

        return redirect()->route('buyer_login');
    }
}
