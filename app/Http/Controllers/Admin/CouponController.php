<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\CouponType;
use App\Model\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CouponController extends Controller
{
    public function index() {
        $coupons = Coupon::orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->paginate(10);

        return view('admin.dashboard.coupon.index', compact( 'coupons'))->with('page_title', 'Promo codes');
    }

    public function addPost(Request $request) {
        $rules = [
            'name' => 'required|max:191',
            'description' => 'nullable|max:191',
        ];

        if ($request->type != CouponType::$FREE_SHIPPING)
            $rules['amount'] = 'required|numeric';

        $request->validate($rules);

        $amount = null;

        if ($request->type != CouponType::$FREE_SHIPPING)
            $amount = $request->amount;

        Coupon::create([
            'name' => $request->name,
            'type' => $request->type,
            'amount' => $amount,
            'user_id' => Auth::user()->id,
            'multiple_use' => $request->multipleUse,
            'description' => $request->description,
        ]);

        return redirect()->route('admin_coupon')->with('message', 'Coupon Added!');
    }

    public function editPost(Request $request) {
        $rules = [
            'name' => 'required|max:191',
            'description' => 'nullable|max:191',
        ];

        if ($request->type != CouponType::$FREE_SHIPPING)
            $rules['amount'] = 'required|numeric';

        $request->validate($rules);

        $coupon = Coupon::where('id', $request->couponId)->first();

        $amount = null;

        if ($request->type != CouponType::$FREE_SHIPPING)
            $amount = $request->amount;

        $coupon->name = $request->name;
        $coupon->type = $request->type;
        $coupon->amount = $amount;
        $coupon->multiple_use = $request->multipleUse;
        $coupon->description = $request->description;
        $coupon->save();

        return redirect()->route('admin_coupon')->with('message', 'Coupon Updated!');
    }

    public function delete(Request $request) {
        Coupon::where('id', $request->id)->delete();
    }
}
