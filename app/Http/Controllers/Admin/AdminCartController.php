<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\Role;
use App\Model\CartItem;
use App\Model\Item;
use App\Model\MetaVendor;
use App\Model\PromoCodes;
use App\Model\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;

class AdminCartController extends Controller
{
    public function addToCart(Request $request)
    {
//        $item = Item::where('status', 1)->where('id', $request->itemId)->first();

        $sessionUser = session('order_customer_id' , null);

        $cartItem = CartItem::where([
            'user_id' => $sessionUser,
            'item_id' => $request->itemId
        ])->get()->first();

        if ($cartItem != null) {
            $cartItem->quantity += $request->qty;
            $cartItem->save();
        } else {
            $cartModel = new CartItem();
            $cartModel->user_id = $sessionUser;
            $cartModel->item_id = $request->itemId;
            $cartModel->quantity = $request->qty;
            $cartModel->created_at = Carbon::now();
            $cartModel->save();
        }

        $count = CartItem::where('user_id', $sessionUser)->get()->sum('quantity');
        return response()->json(['status' => 2000, 'count' => $count]);
    }

    public function addToCartPromo(Request $request)
    {
        $input = $request->input();

        // $check = PromoCodes::where('name', $input['code'])->get()->first();
        $check = Coupon::where('name', $input['code'])->get()->first();
        if ( $check == null ) {
            session()->forget('promo');
            return redirect()->back()->withErrors(['error' => 'Invalid Promo Code']);
        }
        if ( $check->multiple_use == 1 ) {
            session()->push('promo', $check->toArray());
            return redirect()->back()->withErrors(['message' =>  'Promo code successfully added']);
        }
        else {
            // Here Promo code should apply one time
            // Check this user has used this promo
            $checkPromoUsed = Order::where('user_id', Auth::user()->id)->where('promo_code_id', $check->id)->get();
            if ( $checkPromoUsed->isEmpty() ) {
                // So he has not used this promo code
                session()->push('promo', $check->toArray());
                return redirect()->back()->withErrors(['message' =>  'Promo code successfully added']);
            }
            else {
                // User has used this promo code
                return redirect()->back()->withErrors(['error' => 'You have Used This Promo Code Once!']);
            }
        }
    }

    public function addToCartSuccess()
    {
        return back()->with('message', 'Added to cart.');
    }

    public function showCart()
    {
        $sessionUser = session('order_customer_id' , null);

        $cartItems = CartItem::where('user_id', $sessionUser)
            ->with('item', 'color')
            ->get()->toArray();

        if ( count($cartItems) == 0 ) {
            session()->forget('promo');
        }

        $subTotal = 0;
        $promo = 0;
        $giftCard = 0;
        $total = 0;
        foreach ($cartItems as $cart){
            $eachPrice = (int)$cart['quantity'] * (float)$cart['item']['price'];
            $subTotal = $subTotal + $eachPrice;
        }

        if ( session()->has('promo')) {
            $promoType = session('promo')[0]['type'];
            $promo_amount = session('promo')[0]['amount'];
            if ( $promoType == 1 ) { // Fixed amount will be deduct
                $promo = $promo_amount;
            }
            elseif ( $promoType == 2 ) { // Percentage amount will be deduct
                $promo = ( $subTotal * $promo_amount ) / 100;
            }
        }

        $total = $subTotal-$promo-$giftCard;

        return view('admin.dashboard.new_order.cart', compact('cartItems', 'total', 'subTotal','promo','giftCard'))->with('page_title', 'Admin Order Cart');
    }

    public function updateCart(Request $request)
    {
        $sessionUser = session('order_customer_id' , null);

        $input = $request->input();

        $notRemoved = [];
        if(isset($input['cart'])){
            foreach ($input['cart'] as $cart){
                $check = CartItem::where('item_id', $cart['itemId'])->where('user_id', $sessionUser)->get()->first();
                if($check != null){
                    $item = Item::where('id', $cart['itemId'])->get()->first();
                    if ($cart['qty'] < 1){
                        return redirect()->back()->withErrors(['error'=> 'Minimum quantity is 1 for any product']);
                    }
                    if ($item->min_qty > 0 && $item->min_qty < $cart['qty']){
                        return redirect()->back()->withErrors(['error' => $item->name . ' max order qty is ' . $item->min_qty]);
                    }
                    if($item->min_qty < 1 || $item->min_qty == null){

                    } else {
                        $notRemoved[] = $check->id;
                        $check->quantity = $cart['qty'];
                        $check->save();
                    }
                }
            }
        }
        CartItem::whereNotIn('id', $notRemoved)->where('user_id', $sessionUser)->delete();
        return redirect()->back()->withErrors(['message' =>  'Cart has been updated successfully']);
        //return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function updateCartSuccess()
    {
        return back()->with('message', 'Cart Updated!');
    }

    public function deleteCart(Request $request)
    {
        CartItem::where('id', $request->id)->delete();
    }

    public function deleteCartAll(Request $request)
    {
        CartItem::where([])->delete();
    }
}
