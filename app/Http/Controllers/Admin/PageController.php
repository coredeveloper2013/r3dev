<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\PageEnumeration;
use App\Model\Page;
use App\Model\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PageController extends Controller
{
    public function index($id) {
        $page = Page::where('page_id', $id)->first();

        if (!$page) {
            $page = Page::create([
                'page_id' => $id,
            ]);
        }

        $title = 'Page - ';

        if ($id == PageEnumeration::$ABOUT_US)
            $title .= 'Our Story';
        else if ($id == PageEnumeration::$CONTACT_US)
            $title .= 'Contact Us';
        else if ($id == PageEnumeration::$PRIVACY_POLICY)
            $title .= 'Privacy Policy';
        else if ($id == PageEnumeration::$RETURN_INFO)
            $title .= 'Return Info';
        else if ($id == PageEnumeration::$BILLING_SHIPPING_INFO)
            $title .= 'Billing & Shipping Info';
        else if ($id == PageEnumeration::$FAQ)
            $title .= 'FAQs';
        else if ($id == PageEnumeration::$TERMS_CONDITIONS)
            $title .= 'Terms And Conditions';
        else if ($id == PageEnumeration::$COOKIES_POLICY)
            $title .= 'Cookies Policy';

        return view('admin.dashboard.page.index', compact('page'))->with('page_title', $title);
    }

    public function save(Request $request, $id) {
        Page::where('page_id', $id)->update([
            'content' => $request->page_editor,
        ]);

        return redirect()->back()->with('message', 'Updated!');
    }

    public function showCatOneData()
    {
        $featured = 1;
        $title = "Section One";
        $sectionTitle = DB::table('featured')->where('id', $featured)->first();
        $products = Item::with('images')
                        ->where('status', 1)
                        ->where('min_qty', '>', 0)
                        ->where('featured', $featured)
                        ->paginate(10);

        return view('admin.dashboard.page.featured_page', compact('products', 'sectionTitle','featured'))->with('page_title', $title);
    }
    
    public function showCatTwoData()
    {
        $featured = 2;
        $title = "Section Two";
        $sectionTitle = DB::table('featured')->where('id', $featured)->first();
        $products = Item::with('images')
                        ->where('status', 1)
                        ->where('min_qty', '>', 0)
                        ->where('featured', $featured)
                        ->paginate(10);

        return view('admin.dashboard.page.featured_page', compact('products', 'sectionTitle','featured'))->with('page_title', $title);
    }
    
    public function showCatThreeData()
    {
        $featured = 3;
        $title = "Section Three";
        $sectionTitle = DB::table('featured')->where('id', $featured)->first();
        $products = Item::with('images')
                        ->where('status', 1)
                        ->where('min_qty', '>', 0)
                        ->where('featured', $featured)
                        ->paginate(10);

        return view('admin.dashboard.page.featured_page', compact('products', 'sectionTitle','featured'))->with('page_title', $title);
    }

    public function storeCatOneData(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required'
        ]);

        $title = $request->title;

        $insertData = [
            'title' => $title
        ];

        // Check this customer rate it before it or not
        $get_table_info = DB::table('featured')
                                ->where('id', $request->id)
                                ->get()
                                ->toArray();
        if ( count($get_table_info) > 0 ) {
            DB::table('featured')->where('id', $request->id)->update($insertData);
        }
        else {
            DB::table('featured')->insert($insertData);
        }

        return redirect()->back();
    }
}
